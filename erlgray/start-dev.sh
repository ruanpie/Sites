#!/bin/sh

ERL_LIBS=/opt/local/lib/erlang/

# NOTE: mustache templates need \ because they are not awesome.
exec erl -pa ebin edit deps/*/ebin -boot start_sasl \
    -sname erlgray_dev \
    -s erlgray \
    -s reloader
