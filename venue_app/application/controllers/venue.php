<?php

class Venue extends CI_Controller {

  public function __construct(){
    parent::__construct();
    if (!$this->tank_auth->is_logged_in()) {
			redirect('/auth/login/');
		}
    $this->load->model('venue_model');
  }

  public function all($offset=0){
    $data['title'] = 'Venue';

    $this->form_validation->set_rules('search_name');
    $search = false;
    if($this->form_validation->run() !== FALSE ){

      if( isset($_POST['search_name']) && isset($_POST['from_date_time']) && isset($_POST['to_date_time']) ){
        $search_name    = $this->input->post('search_name');
        $from_date_time = $this->input->post('from_date_time');
        $to_date_time   = $this->input->post('to_date_time');
        $_SESSION['search_name'] = $search_name;
        $_SESSION['from_date_time'] = $from_date_time;
        $_SESSION['to_date_time'] = $to_date_time;
        $search = true;
      }elseif( isset($_SESSION['search_name']) && isset($_SESSION['from_date_time']) && isset($_SESSION['to_date_time']) ){
        $search_name    = $_SESSION['search_name'];
        $from_date_time = $_SESSION['from_date_time'];
        $to_date_time   = $_SESSION['to_date_time'];
      }

    }

    if($search){
      $config['total_rows'] = $this->venue_model->count_search($search_name,$from_date_time,$to_date_time); // FILTER
      $data['venues'] = $this->venue_model->listall_search($offset,$search_name,$from_date_time,$to_date_time);  // FILTER
    }else{
      $config['total_rows'] = $this->venue_model->count();
      $data['venues'] = $this->venue_model->listall($offset);
    }


    $config['base_url'] = site_url("venue");
    $this->pagination->initialize($config);

    $data['venue_pagination'] = $this->pagination->create_links();

    $this->load->view("template/header", $data);
    $this->load->view("venue/header", $data);
    $this->load->view("venue/index", $data);
    $this->load->view("template/footer", $data);

  }

  public function create(){
    $data['title'] = 'Create Venue';
    $this->load->view("template/header", $data);
    $this->load->view("venue/header", $data);
    // $this->load->view("venue/success", $data);

    // $this->load->view("venue/create", $data);

    $this->load->view("template/footer", $data);


  }

}
