<?php

class Venue_model extends CI_Model {

  public function count(){
    return $this->db->count_all('venue');
  }

  public function count_search($search_name,$from_date_time,$to_date_time){
    $this->db->like('name',$search_name);
    $this->db->from('venue');
    return $this->db->count_all_results();
  }
  
  public function listall($offset){
    $query = $this->db->get('venue',20,$offset);
    return $query->result_array();
  }

  public function listall_search($offset,$search_name,$from_date_time){
    $this->db->select("*");
    $this->db->like('name', $search_name);
    $this->db->from("venue");
    $query = $this->db->get_where('',array(),20,$offset);
    return $query->result_array();
  }

  public function search($offset,$name,$from_date_time,$to_date_time){
    //array(
    //  'name'=>$name,
    //  'date_time BETWEEN '=>
    //);
    
    $this->db->select("*");
    $this->db->like('name LIKE ', $name);
    $this->db->or_where('id >', $id);
    $query = $this->db->get_where('venue',20,array(),$offset);

    // Produces: WHERE name != 'Joe' OR id > 50

    $query->db->get_where('venue',$array,20,$offset);
    return $query->result_array();
  }

}

?>