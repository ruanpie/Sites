<?php
$attributes = array('id' => 'venue_form');
echo form_open('venue', $attributes);
?>

<table>
  <tr>
    <th>Name</th><th>From Date Time</th><th>To Date Time</th>
  </tr>
  <tr>
    <td><input type="text" id="search_name" name="search_name" value="<?php echo $search_name ?>" /></td>
    <td> <input type="text" id="from_date_time" name="from_date_time" /> </td>
    <td> <input type="text" id="to_date_time" name="to_date_time" /> </td>
    <td> <button>Search</button> </td>
    <td> <button type="button" onclick="clear_venue_search();">Clear</button> </td>
  </tr>
</table>

<form>

<hr />

<p>
  <?php echo $venue_pagination; ?>
</p>

<table>
  <tr>
    <th>Name</th><th>Start</th><th>Detail</th>
  </tr>
<?php
foreach($venues as $venue){
?>
  <tr>
    <td><?php echo $venue['name'] ?></td>
    <td><?php echo $venue['start_datetime'] ?></td>
    <td><a href="<?php echo base_url("venue/detail/".$venue['id']); ?>"><img src="<?php echo base_url("res/images/view.png"); ?>" /></a></td>
  </tr>
<?php
}
?>
</table>

<p>
  <?php echo $venue_pagination; ?>
</p>

<hr />