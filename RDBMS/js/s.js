// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
var database_name, table_name, col_name, col_value, amount_columns, edit_button_id, action_status, parent_node, toggle_button, toggle_status, object, tbl_num , previous, elem_text, elem, html, RecordAmount;
// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
var nmr = 0;

// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
function select_database(database_name, parent_node){
	var SelectDatabaseRequest = new Request({
		method:'get',
		url:'mod/get_tables.php?dbname='+database_name,
		onRequest:function(){
		  show_loader();
		  $('record_table').set('html','');
		},
		onComplete:function(response){
      $(parent_node).set('html',$(parent_node).innerHTML+response);
      hide_loader();
		}
	}).send();
}
// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
function get_databases(){
  var SelectTableRequest = new Request({
		method:'get',
		url:'mod/get_databases.php',
		onRequest:function(){
		  show_loader();
    },
		onComplete:function(response){
			$('sel_database').set('html',response);
			hide_loader();
		}
  }).send();
}
// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
function select_table(table_name){
	var SelectTableRequest = new Request({
		method: 'get',
		url: 'mod/get_entries.php?tblname='+table_name,
		onRequest:function(){
		  show_loader();
		},
		onComplete: function(response){
			$('record_table').set('html',response);
			hide_loader();
		}
	}).send();
}
// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
function expand(database_name,parent_node,toggle_button){
	collapse_all_expanded();
	select_database(database_name,parent_node);
	$(toggle_button).set('src','img/min.png');
	$(toggle_button).set('onclick','collapse(\''+parent_node+'\',\''+database_name+'\',this.id)');
}
// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
function collapse(object,database_name, toggle_button){
  $('record_table').set('html','');
	$(object).set('html', database_name);
	$(toggle_button).set('src','img/plus.png');
	$(toggle_button).set('onclick', 'expand(\''+database_name+'\',this.parentNode.nextSibling.id,this.id)');
}
// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
function collapse_all_expanded(){
	var object = '';
	var c = 0 ;
	function create_elem_str(c){
		object = "database_td_"+c;
		object = $(object);
		return object;
	}
	c++;
	while(create_elem_str(c)){
		try {
			if(object.firstChild.nextSibling){
				var new_elem_str = create_elem_str(c);
				var all_html = new_elem_str.innerHTML;
				var dbn = all_html.substring(0,all_html.indexOf("<"));
				new_elem_str.set('html', dbn); //and change picture
				var expand_image = "toggle_"+dbn;
				$(expand_image).src = "img/plus.png";
			}
		}catch(e){
		  alert("Collapse all expanded function broke");
			break;
		}
		c++;
	}			
}
// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
function s_t(table_name,tbl_num){
  select_table(table_name);
  if($('table_selected').value == ''){
    change_table_name_colour(tbl_num,"red");
  }else{
	  change_table_name_colour(tbl_num,"red");
	  change_table_name_colour($('table_selected').value,"white");
  }
  $('table_selected').value = tbl_num;
}
// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
function change_table_name_colour(tbl_num,colour){
	$("table_td_"+tbl_num).style.backgroundColor = colour;
}
// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
function show_loader(){ $('loader').style.display = ''; }
function hide_loader(){ $('loader').style.display = 'none'; }
// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

// Sql
function start_insert_record(RecordAmount,ColumnCount){
  if(RecordAmount == 0 || RecordAmount == ''){
    alert("insert number here");
    $('insert_record_amount').focus();
  }else{
     
    var inserting_records = "" ;
    for(var a = 0 ; a < RecordAmount; a++){
      inserting_records = inserting_records + "<tr>";
      for(var b = 0 ; b< ColumnCount; b++){
        inserting_records = inserting_records + "<td><input type=\"text\" ></td>";
      }
      inserting_records = inserting_records + "</tr>";
    }
    $('ins_area_2').set('html',$('ins_area_2').innerHTML+inserting_records);
    
    $('ins_area_1').style.display = '';
    $('ins_area_2').style.display = '';
    
  }
//  var InsertRecord = new Request({
//    method: 'get',
//    url: 'mod/ins_sql.php?db='+Db+"&tbl="+Tbl+"column="+ColumnData,
//    onRequest:function(){
//      show_loader();
//    },
//    onComplete:function(response){
//      select_table(Tbl);
//      hide_loader();
//    }
//  }).send();
}


//
//function delete_record(Db,Tbl,Columns){
//  
//}
//
//function update_record(Db,Tbl,Columns){
//  
//}

// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
