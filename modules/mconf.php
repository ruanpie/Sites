<?php

require("../../webproject/conf/constants.php");
require("../../webproject/conf/paths.php");
require("../../webproject/conf/dbsettings.php");

session_start();
ini_set('session.gc_maxlifetime',8*60*60);
putenv("TZ=Africa/Johannesburg");
putenv("date.timezone=Africa/Johannesburg");

require(constant('FRAMEWORK_CLASSES').'require_core.php');

$current_page = common::get_currecnt_page();
$location = constant('ACTIVE_PATH');

$cdate       = new cdate();
$s           = new schema();
$b           = new button();
$p           = new pages();
$userdetails = new userdetails();
$r           = new render();
$g           = new group();
$l           = new auth();
$cfile       = new cfile();

?>
