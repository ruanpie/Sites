<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There area two reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router what URI segments to use if those provided
| in the URL cannot be matched to a valid route.
|
*/

$route['album']        = "album_ctrl/view";
$route['album/(:num)'] = "album_ctrl/view/$1";
$route['album/(:any)'] = "album_ctrl/gallery/$1";
$route['album/create'] = "album_ctrl/create";

$route['upload']              = "album_upload_ctrl/upload";
$route['album_upload/(:any)'] = "album_upload_ctrl/upload_form/$1";

$route['photo']                       = "album_ctrl/view";
$route['photo/edit/(:num)']           = "photo_ctrl/edit/$1";
$route['photo/album_cover_id/(:num)'] = "photo_ctrl/save_cover_id/$1";
$route['photo/delete']                = "album_ctrl/view";
$route['photo/delete/(:num)']         = "photo_ctrl/del/$1";
$route['photo/delete/(:any)']         = "album_ctrl/view";
$route['photo/save']                  = "photo_ctrl/save";

$route['auth']                  = 'auth/login';
$route['auth/forgot_password']  = 'auth/login';
$route['auth/(:any)']           = 'auth/$1';
$route['logout']                = 'auth/logout';

$route['profile']        = 'profile_ctrl/view';
$route['profile/(:any)'] = 'profile_ctrl/$1';

$route['public_album']        = 'public_album_ctrl/view';
$route['public_album/(:num)'] = 'public_album_ctrl/view/$1';
$route['public_album/(:any)'] = 'public_album_ctrl/gallery/$1';

$route['default_controller']  = "ctrl/view";
$route['(:any)']              = "ctrl";// Cause a 404

$route['404_override'] = 'error';




//print_r($route);



/* End of file routes.php */
/* Location: ./application/config/routes.php */