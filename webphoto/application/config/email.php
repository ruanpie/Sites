<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| Email
| -------------------------------------------------------------------------
| This file lets you define parameters for sending emails.
| Please see the user guide for info:
|
|	http://codeigniter.com/user_guide/libraries/email.html
|
*/

//$config['protocol']    = 'smtp';
//$config['smtp_host']    = 'ssl://smtp.gmail.com';
//$config['smtp_port']    = '465';
//$config['smtp_timeout'] = '7';
//$config['smtp_user']    = 'webphoto12345@gmail.com';
//$config['smtp_pass']    = 's0m3p4ssw0rd';

$config['protocol'] = 'sendmail';
$config['mailpath'] = '/usr/sbin/sendmail';
$config['charset'] = 'iso-8859-1';
$config['wordwrap'] = TRUE;

$config['mailtype'] = 'html';
$config['charset'] = 'utf-8';
$config['newline'] = "\r\n";


/* End of file email.php */
/* Location: ./application/config/email.php */