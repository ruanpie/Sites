<?php
if($authenticated){
  ?><a href="<?php echo base_url("album/create"); ?>">Create Album</a><?php
}
?>
<h2>
  Albums
</h2>
<?php echo $album_pagination; ?>
<table>
  <tr>
    <?php
      $col=0;
      foreach($albums as $album){
        $col++;
        if( $album['cover_name'] == ''){
          $cover=base_url("res/img/photo_album.jpg");
        }else{
          $cover=base_url("uploads/thumbs/".photo_thumb_name($album['cover_name']));
        }
    ?>
    <td style="text-align: center;">
      <div>
      <a href="<?php echo base_url("album/".urlencode($album['name'])); ?>" >
        <p>
        <?php
          if(strlen($album['name']) > 20){
            echo substr($album['name'],0,19).'...';
          }else{
            echo $album['name'];
          }
        ?>
        </p>
        <img src="<?php echo $cover; ?>" alt="Title #0" />
      </a>
      </div>
      <?php
        if ( $album_counts[$album['id']] <= 0 ) {
          print 'No Photos';
        }elseif ( $album_counts[$album['id']] == 1 ) {
          print '1 Photo';
        } elseif ( $album_counts[$album['id']] > 1 ) {
          print $album_counts[$album['id']].' Photos';
        }
      ?>
    </td>
    <?php
    if($col==5){
      print '</tr><tr>';
      $col=0;
    }
  }
  if($col>0){
    print '</tr>';
  }
?>
</table>
<br />
<?php echo $album_pagination; ?>