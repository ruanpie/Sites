<h2>
  <?php echo $album_name; ?>
</h2>
<a href="<?php echo base_url("album_upload/".$album_name); ?>">Upload</a>
<div id="gallery" class="content">
  <div id="controls" class="controls"></div>
  <div class="slideshow-container">
    <div id="loading" class="loader"></div>
    <div id="slideshow" class="slideshow"></div>
  </div>
  <div id="caption" class="caption-container"></div>
</div>
<div id="thumbs" class="navigation">
  <ul class="thumbs noscript">
    <?php
      foreach($photos as $photo){
      ?>
        <li>
          <a class="thumb" href="<?php echo base_url('uploads/'.$photo['name']); ?>" >
            <img src="<?php echo base_url("uploads/thumbs/".photo_thumb_name($photo['name'])); ?>" />
          </a>
          <div class="caption">
            <!--<div class="download">
              <a href="#">Download Original</a>
            </div>-->
            <br/>
            <div class="download">
              <a href="<?php echo base_url('photo/edit/'.$photo['id']); ?>">Edit</a>
            </div>
            <div class="image-title"><?php echo $photo['title']; ?></div>
            <div class="image-desc"><?php echo $photo['description']; ?></div>
          </div>
          <script type="text/javascript">$('#caption').css("display",'');</script>
        </li>
      <?php
      }
    ?>
  </ul>
</div>