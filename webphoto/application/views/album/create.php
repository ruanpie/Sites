<h2>
  Create Album
</h2>
<form method="post" action="<?php echo base_url("album/create"); ?>" >
  <h5>Album name</h5>
  <?php echo form_error('album_name'); ?>
  <input type="text" id="album_name" name="album_name" value="<?php echo set_value('album_name'); ?>" size="50" />  
  <br /><br />
  <button id="create_btn">Create</button>
</form>