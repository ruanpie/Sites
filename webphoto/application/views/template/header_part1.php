<!DOCTYPE HTML>
<html lang="en">
  <head>
    <meta http-equiv="Content-type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width">
    <!-- Force latest IE rendering engine or ChromeFrame if installed -->
    <!--[if IE]><meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"><![endif]-->
      
    <title><?php echo constant('MY_SITE_TITLE'); ?></title>
    
    <link rel="icon" type="image/ico" href="<?php echo base_url("/res/img/favicon.ico"); ?>" />
    
    <link rel="stylesheet" href="<?php echo base_url("res/css/main.css"); ?>" type="text/css" />
    
    <!--galleriffic-->
    <link rel="stylesheet" href="<?php echo base_url("res/css/basic.css"); ?>" type="text/css" />
		<link rel="stylesheet" href="<?php echo base_url("res/css/galleriffic.css"); ?>" type="text/css" />
    
		<script type="text/javascript" src="<?php echo base_url("res/js/jquery-1.3.2.js"); ?>"></script>
		<script type="text/javascript" src="<?php echo base_url("res/js/jquery.history.js"); ?>"></script>
		<script type="text/javascript" src="<?php echo base_url("res/js/jquery.galleriffic.js"); ?>"></script>
		<script type="text/javascript" src="<?php echo base_url("res/js/jquery.opacityrollover.js"); ?>"></script>
		
		<script type="text/javascript">
			document.write('<style>.noscript { display: none; }</style>');
		</script>
    <!-- / galleriffic-->
    
  </head>
  <body>
    
    <div id="page">
			<div id="container">
				<h1><a href="<?php echo base_url(""); ?>"><?php print constant('MY_SITE_NAME'); ?></a></h1>
        
        <p>