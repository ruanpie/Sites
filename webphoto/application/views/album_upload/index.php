<h2>
  Upload
</h2>
<form action="<?php echo base_url("upload"); ?>" method="post" enctype="multipart/form-data">
  Photos to <a href="<?php echo base_url("album/".$album_name); ?>"><?php echo $album_name; ?></a>:<br />
  <br/>
  <input id="photos" name="photos[]" id="photos" type="file" multiple="" />
  <br/><br/>
  <input id="upload_btn" type="submit" value="upload" />
  <?php echo $error;?>
  <input type="hidden" name="album_id"   value="<?php echo $album_id; ?>" />
  <input type="hidden" name="album_name" value="<?php echo $album_name; ?>" />
  
  <br />
    <div id="upload_size" style="display: none;"></div>

    <script type="text/javascript">
      $("input[type=submit]").attr("disabled", "disabled");
      var allowed_total=<?php echo (int)(ini_get('upload_max_filesize')); ?>;
      var allowed_total_files=<?php echo (int)(ini_get('max_file_uploads')); ?>;
      $('#photos').bind('change', function(){
        
        if( this.files.length <= 20 ){
          var total_size = 0;
          for(var a = 0; a < this.files.length; a++){
            total_size += this.files[a].size/1024/1024;
          }
          total_size = Math.round(total_size);
          if( total_size > allowed_total ){
            $('#upload_size').html('You are trying to upload '+total_size+'Mb.<br/> The upload maximum is '+allowed_total+'Mb.<br/>Please reduce upload size.');
            $('#upload_size').attr('class','error');
            $('#upload_size').show();
          }else{
            $('#upload_size').html('Upload size : '+total_size+'Mb.');
            $('#upload_size').show();
            $("input[type=submit]").removeAttr("disabled");
            $('#upload_size').attr('class','');
          }
        }else{
          $('#upload_size').html('You are trying to upload too many files. Choose less files.');
          $('#upload_size').attr('class','error');
          $('#upload_size').show();          
        }
        
      });
    </script>
  
</form>