<?php

class Menu_model extends CI_Model {
  
  public function __construct(){
  }
  
  public function count(){
    return $this->db->count_all('menu');
  }

  public function open($menu_id){
    $query = $this->db->get_where('menu',array('id' => $menu_id),1);
    if( $query->num_rows > 0 ){
      return $query->result_array();
    }else{
      return FALSE;
    }
  }
  
  public function listall($offset){
    $query=$this->db->get('menu',20,$offset);
    return $query->result_array();
  }
  
  public function create(){
    $data = array(
      'name'        => $this->input->post('name')
    );
    
    $this->db->insert('menu', $data);
    return $this->db->insert_id();
  }

  public function update($menu_id){
   $data = array(
      'name'        => $this->input->post('name')
    );
    $this->db->where('id',$menu_id);
    return $this->db->update('menu',$data);
  }
  
  public function delete($menu_id){
    // check whether to delete or not
    
    //$this->db->delete('menu',array('id'=>$menu_id));
    
  }
  
  public function get_menu(){
    $menu = array();
    $query = $this->db->get_where('menu',array('root_item_id' => NULL));
    $rows = $query->result_array();
    for($a=0; $a < $query->num_rows; $a++){
      $item_array = array('id'=>$rows[$a]['id'],'text'=>$rows[$a]['text'],'href'=>$rows[$a]['href']);
      $child_array = menu_model::get_children($rows[$a]['id']);
      if($child_array){
        $menu[$a] = array_merge($item_array,$child_array);
      }else{
        $menu[$a] = $item_array;
      }
    }
    return $menu;
  }
    
  public function get_children($root_id){
    $children_query = $this->db->get_where('menu',array( 'root_item_id' => $root_id ));
    $children_num_rows = $children_query->num_rows();
    if($children_num_rows>0){
      $children_rows = $children_query->result_array();
      $children_array = array();
      foreach( $children_rows as $child_row ){
        $sub_array = menu_model::get_children($child_row['id']);
        $child_item_array = array('id'=>$child_row['id'],'text'=>$child_row['text'],'href'=>$child_row['href']);
        if(is_array($sub_array)){
          $item_array = array_merge($child_item_array,$sub_array);
        }else{
          $item_array = $child_item_array;
        }
        array_push($children_array,$item_array);
      }
      return $children_array;
    }else{
      return false;
    }
  }

  
}