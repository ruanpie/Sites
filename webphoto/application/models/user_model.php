<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class User_model extends CI_Model {
  
  public function __construct(){
  }
  
  public function open($id){
    $query = $this->db->get_where('users',array('id' => $id),1);
    if( $query->num_rows > 0 ){
      return $query->result_array();
    }else{
      return FALSE;
    }
  }
  
}