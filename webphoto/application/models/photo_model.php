<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Photo_model extends CI_Model {
  
  public function __construct(){
  }
  
  public function all(){
    $this->db->order_by('timestamp','asc');
    $query = $this->db->get('photo');
    return $query->result_array();
  }
  
  public function count(){
    return $this->db->count_all("photo");
  }

  public function open($id){
    $query = $this->db->get_where('photo',array('id' => $id),1);
    if( $query->num_rows > 0 ){
      return $query->result_array();
    }else{
      return FALSE;
    }
  }
  
  public function album_photos($album_id){
    $this->db->select("photo.*");
    $this->db->from(" photo ");
    $this->db->join("album", "photo.album_id = album.id", 'left outer');
    $query = $this->db->get_where('',array('album.id' => $album_id));
    if( $query->num_rows > 0 ){
      return $query->result_array();
    }else{
      return FALSE;
    }
  }
  
  public function listall($offset){
    $query=$this->db->get('photo',20,$offset);
    return $query->result_array();
  }
  
  public function create(){
    $name = $this->input->post('name');
    $album_id = $this->input->post('album_id');
    $this->create_details($name,$album_id);
  }
  
  public function create_details($name,$album_id){
    $data = array(
      'album_id' => $album_id,
      'name'     => $name
    );
    $this->db->insert('photo',$data);
    return $this->db->insert_id();
  }

  public function update($id){
   $data = array(
      'name' => $this->input->post('')
    );
    $this->db->where('name',$id);
    return $this->db->update('photo',$data);
  }
  
  public function delete($id){
    $this->db->delete('photo',array('id'=>$id));
  }
  
  public function update_title_desc($photo_id){
    $data = array(
      'title'       => $this->input->post('title'),
      'description' => $this->input->post('description')
    );
    $this->db->where('id',$photo_id);
    return $this->db->update('photo',$data);
  }
  
}