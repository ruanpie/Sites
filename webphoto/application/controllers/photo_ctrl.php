<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Photo_ctrl extends CI_Controller {

  function __construct(){				
	  parent::__construct();
    
    if (!$this->tank_auth->is_logged_in()) {
			redirect('/auth/login/');
		}
    
    $this->load->model('photo_model');
    $this->load->model('album_model');
  }

	public function edit($id=0){
    $data['photo_id']=$id;
    $photo = $this->photo_model->open($id);
    if( $photo === FALSE ){
      show_404();
    }else{
      $data['photo_name']         = $photo[0]['name'];
      $data['photo_title']        = $photo[0]['title'];
      $data['photo_description']  = $photo[0]['description'];
      $this->load->view("template/header",$data);
      $this->load->view('photo/edit', $data);
      $this->load->view("template/footer",$data);
    }
	}
  
  public function save_cover_id($photo_id){
    $photo = $this->photo_model->open($photo_id);
    if( $photo === FALSE ){
      show_404();
    }else{
      $this->album_model->save_cover($photo_id,$photo[0]['album_id']);
      redirect('/photo/edit/'.$photo_id, 'refresh');
    }
  }
  
  public function del($id){
    $photo = $this->photo_model->open($id);
    $gallery_image = $_SERVER['DOCUMENT_ROOT'].'/webphoto/uploads/'.$photo[0]['name'];
    $thumb_image = $_SERVER['DOCUMENT_ROOT'].'/webphoto/uploads/thumbs/'.photo_thumb_name($photo[0]['name']);
    if( get_file_info($gallery_image) != FALSE ){
      delete_files($gallery_image);
    }
    if( get_file_info($thumb_image) != FALSE ){
      delete_files($thumb_image);
    }
    // keep original image...
    $album = $this->album_model->open($photo[0]['album_id']);
    $album_name = $album[0]['name'];
    $this->photo_model->delete($id);
    //print_r($album);
    // Switch to default image when album has no images.
    if( $this->album_model->album_photo_count($album[0]['id']) == 0 ){
      $this->album_model->save_cover(0,$album[0]['id']);
    }
    
    redirect('/album/'.$album_name, 'refresh');
  }
  
  public function save(){
    $photo_id = $this->input->post('photo_id');
    $this->photo_model->update_title_desc($photo_id);
    redirect('/photo/edit/'.$photo_id, 'refresh');
  }
  
}

?>