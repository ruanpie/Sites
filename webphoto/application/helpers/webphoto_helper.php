<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if ( ! function_exists('photo_thumb_name'))  {
  function photo_thumb_name($name){ // $name = 'photo.jpg'
    $array=explode('.',$name);
    $ext=$array[count($array)-1];
    $file_no_ext=array_pop($array);
    return implode('',$array).'_s.'.$ext;
  }
}

if ( ! function_exists('photo_orig_name'))  {
  function photo_orig_name($name){
    // add /orig/
    return $name;
  }
}

if( ! function_exists('extention_name') ){
  function extention_name($name){
    $array=array_reverse(explode('.',$name));
    return strtolower( $array[0] );
  }
}

if( ! function_exists('filename_no_ext') ){
  function filename_no_ext($filename){
    $array=explode('.',$filename);
    $name_only = array_shift($array);
    return $name_only;
  }
}

if( ! function_exists('trim_filename_stuff') ){
  function trim_filename_stuff($filename){
    $trimed = str_ireplace(' ','',filename_no_ext($filename)).'.'.extention_name($filename);
    return $trimed;
  }
}

?>