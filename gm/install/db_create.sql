
CREATE TABLE IF NOT EXISTS `gm_inventory_type` (
	`Id` int(9) NOT NULL AUTO_INCREMENT,
	`Description` varchar(200) NOT NULL,
	PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=UTF8;

CREATE TABLE `gm_quantity` (
	`Id`		int(9) NOT NULL AUTO_INCREMENT,
	`label`	varchar(100) NOT NULL,
	PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=UTF8;

CREATE TABLE IF NOT EXISTS `gm_inventory` (
	`Id`							int(9) 				NOT NULL AUTO_INCREMENT,
	`TypeId`					int(9) 				NOT NULL,
	`Description`			varchar(255)	NOT NULL,
	`InstructionDesc` varchar(255)	NOT NULL,
	`Quantity`				varchar(100)	NOT NULL,
	`QuantityLabelId`	int(9),
  PRIMARY KEY (`Id`),
  FOREIGN KEY (`TypeId`) 					REFERENCES `gm_inventory_type`(`Id`),
  FOREIGN KEY (`QuantityLabelId`) REFERENCES `gm_quantity`(`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=UTF8;
