<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Todo extends CI_Controller {

  public function __construct(){
    parent::__construct();
    $this->load->model('todo_model');
  }

  public function save(){
    $this->form_validation->set_rules('todo', 'Todo', 'required');
    if ($this->form_validation->run() == FALSE){
      //VIEW
      $data['list'] = $this->todo_model->listall_open($offset=0);
      $config['base_url'] = site_url("view");
      $config['total_rows'] = $this->todo_model->count();
      $this->pagination->initialize($config);
      $data['todo_pagination'] = $this->pagination->create_links();
      $this->load->view("template/header",$data);
      $this->load->view('index', $data);
      $this->load->view("template/footer",$data);
    }else{
      $this->todo_model->save();
      redirect(base_url(), 'refresh');
    }
  }
  
  public function done($id){
    $this->todo_model->done($id);
    redirect(base_url('view'), 'refresh');
  }
  
  public function view($offset=0){
    $data['list'] = $this->todo_model->listall_open($offset);
    $config['base_url'] = site_url("view");
    $config['total_rows'] = $this->todo_model->count();
    $this->pagination->initialize($config);
    $data['todo_pagination'] = $this->pagination->create_links();
    $this->load->view("template/header",$data);
    $this->load->view('index', $data);
    $this->load->view("template/footer",$data);
  }
  
  public function edit($id){
    $this->session->set_userdata(array('todo_id'=>$id));
    $data['edit'] = true;
    $data['todo_id'] = $id;
    $todo = $this->todo_model->open($id);
    $data['id']   = $todo[0]['id'];
    $data['todo'] = $todo[0]['todo'];
    $data['desc'] = $todo[0]['desc'];
    $this->load->view("template/header",$data);
    $this->load->view('edit', $data);
    $this->load->view("template/footer",$data);
  }
  
  public function edit_save(){
    $todo_id = $this->session->userdata('todo_id');
    $this->todo_model->update($todo_id);
    $this->session->unset_userdata('todo_id');
    redirect(base_url(), 'refresh');
  }

}

?>