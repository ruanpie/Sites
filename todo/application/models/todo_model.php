<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Todo_model extends CI_Model {
  
  public function __construct(){
  }
  
  public function open($id){
    $query = $this->db->get_where('todo',array('id' => $id ),1);
    return $query->result_array();
  }
 
  public function listall($offset){
    $query=$this->db->get('todo',constant('PAGINATION_TOTAL'),$offset);
    return $query->result_array();
  }
  
  public function listall_open($offset){
    return $this->listall_status($offset,constant('STATUS_OPEN'));
  }
  
  public function listall_done($offset){
    return $this->listall_status($offset,constant('STATUS_DONE'));
  }
  
  public function listall_status($offset,$status){
    $query = $this->db->get_where('todo',array('status' => $status ),constant('PAGINATION_TOTAL'),$offset);
    return $query->result_array();
  }
  
  public function count(){
    $this->db->like('status', constant('STATUS_OPEN'));
    $this->db->from('todo');
    return $this->db->count_all_results();
  }
  
  public function save(){
    $data = array('todo'=>$this->input->post('todo'), 'desc'=>$this->input->post('desc'));
    $this->db->insert('todo', $data);
    return $this->db->insert_id();
  }
  
  public function done($id){
    $data = array('status' => constant('STATUS_DONE'));
    $this->db->where('id',$id);
    $this->db->update('todo',$data);
    return $this->db->insert_id();
  }
  
  public function delete($id){
    $this->db->delete('todo',array('id'=>$id));
  }
  
  public function update($id){
    $data = array('todo'=>$this->input->post('todo'), 'desc'=>$this->input->post('desc'));
    $this->db->where('id',$id);
    $this->db->update('todo',$data);
    return $this->db->insert_id();
  }
  
}