<?php
check_dependancy("ez_sql_core.php");
//require("ez_sql_postgresql.php");

class cdbcon extends ezSQLcore {
	static $dbcon;
	private $username;
	private $password;
	private $dbname;
	private $host;
	
	function cdbcon(){
		$this->dbconect(constant('DBUSER'),constant('DBPASS'),constant('DBNAME'),constant('DBHOST'));
	}
	
	function dbconect($username,$password,$dbname,$host){
		try{
			$db = new ezSQL_mysql($username,$password,$dbname,$host);
			//$db = new ezSQL_postgresql($username,$password,$dbname,$host);
			$this->is_connected($db);
		}catch (Exception $e){
			// handle this 
		}
		cdbcon::$dbcon = $db;
	}
	
	function is_connected($db){
		if (is_object($db)){ $_SESSION['dbconnected'] = true;
		}else{               $_SESSION['dbconnected'] = false; }
	}
	
	function return_db_con(){
		return cdbcon::$dbcon;
	}
}
?>