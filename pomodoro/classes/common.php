<?php
check_dependancy("cdbcon.php");


class common extends cdbcon {
 private $str;
 private $needle;
 private $del_string;
 private $filename;
 private $current_page;
 private $match_page;
 private $default;
 
  function get_val_with_needle($str,$needle){
	  $pos1 = strpos($str,$needle);
	  $rem = substr($str,$pos1 + 1);
	  $pos2 = strpos($rem,$needle);
	  if($pos1 > 0 && $pos2 > 0){
	    $value_in_needle = substr($str,$pos1 + 1,$pos2);
	    return $value_in_needle;
	  }else{
	    return false;
	  }
	}
	
	function get_str_in_between($Str,$fromStr,$toStr,$includeFromAndTo){
		$from_pos = strpos($Str,$fromStr);
		$rem = substr($Str,$from_pos);
		$to_pos = strpos($rem,$toStr);
		if($includeFromAndTo == true){
			$str_in_between = substr($Str,$from_pos,$to_pos + strlen($toStr));
		}else{
			$str_in_between = substr($Str,$from_pos + strlen($fromStr),$to_pos - strlen($fromStr));
		}
		
		return $str_in_between;
	}
	
	function drop_tbl_partial_string($del_string,$dbuser,$dbpass,$dbhost){
  	$c = mysql_connect($dbhost,$dbuser,$dbpass);
  	$query_ref = mysql_query("SHOW TABLES FROM ".constant('DBNAME')." LIKE '%".$del_string."%'",$c);
  	$NumTables = mysql_num_rows($query_ref);
  	for($a = 0; $a < $NumTables; $a++){
  	  $table_row = mysql_fetch_array($query_ref);
  	  mysql_query("DROP TABLE ".constant('DBNAME').".".$table_row[0]." ",$c);
  	}
  }
  
  public function nav($page){
    header("Location: ".constant("NAV").$page);
  }
  
  public function navparams($page,$params){
  	//header("Location: ".constant("ACTIVE_URL").constant("INCLUDE_FOLDER").$page.$params);
  	//header("Location: ".constant("INCLUDE_FOLDER").$page.$params);
  }
  
  public function nav_path($page){
    if(isset($_GET['devallow'])){
      header("Location: ".$page."?devallow=".$_GET['devallow']);
    }else{
      header("Location: ".$page);
    }
  	
  }
  
  public function get_currecnt_page(){
    $location_array = explode("/",$_SERVER['PHP_SELF']);
    return $location_array[(count($location_array) - 1)];
  }
  
  public function get_current_url(){
  	$location_array = explode("/",$_SERVER['PHP_SELF']);
  	$l = '';
  	for($a = 0; $a < (count($location_array)-1) ; $a++){
  		if($a == 1){
  			$l .= $location_array[$a];
  		}else{
	  		$l .= "/".	$location_array[$a];
  		}
  	}
  	if(@$_SERVER['HTTPS'] == "on"){
  		$protocol = "https://";
  	}else{
  		$protocol = "http://";
  	}
  	return $protocol.$_SERVER['SERVER_ADDR'].":".$_SERVER['SERVER_PORT'].$l;
  }
  
  function file_extension($filename){
		$filePieces = explode(".",$filename);
		return $filePieces[count($filePieces)-1];
  }
  
  function file_name_without_extension($filename){
	  $filePieces = explode(".",$filename);
  	return $filePieces[0];
  }
  
  function redirect_based_on_current_page($current_page,$match_page,$default){
  	if($current_page == $match_page){ $this->nav($match_page);
  	}else{                            $this->nav($default); }
  }
  
  function check_empty_input($input){
  	if(str_ireplace(" ","",$input) == ""){ return false; }
  	else{                                  return true; }
  }
  
  function replace_array($search_array,$replace_array,$text){
  	if( is_array($search_array) && is_array($replace_array) && count($search_array) == count($replace_array) ){
  		$ReturnValue = $text;
  		$array_size = count($search_array);
			for( $c = 0 ; $c < $array_size ; $c++ ){
				$ReturnValue = str_ireplace($search_array[$c],$replace_array[$c],$ReturnValue);
			}
			return $ReturnValue;
  	}
  	return false;
  }
  
	 // tag entered without < > 
  function find_complete_tag($html,$tag){
		$tagPos = strpos($html,$tag);
		$tagStartPos = $tagPos;
		$letter_backwards='';
		while($letter_backwards != "<"){
			$tagStartPos -= 1;
			$letter_backwards = substr($html,$tagStartPos,1);
		}
		$tagEndPos = $tagPos;
		$letter_backwards='';
		while($letter_backwards != ">"){
			$tagEndPos += 1;
			$letter_backwards = substr($html,$tagEndPos,1);
		}
		$length = $tagEndPos - $tagStartPos;
		substr($html,$tagStartPos,$length+1);
	}

/*
	// TODO this does not work that great with nested tags, please improve!!!!!!
	// tag entered without < > 	
	// Both the below functions finds the content between the fist two tags, specified
	function tag_content($html,$tag){ // self enclosing tag
		$tag_start = strpos($html,"<".$tag);
		$c=1;
		do{
	    $chr = substr($html,$tag_start+$c,2);
	    $c++;
	  }while($chr != "/>");
		return substr($html,$tag_start,$c+1);
	}
	
	// tag entered without < > 
	function tag_content2(){ // tag that closes
		
	}
	
	function get_attr($tag,$attr){
		$attr_start = strpos($tag,$tag.$tag)+strlen($attr);
		$c=1;
		do{
	    $chr = substr($tag,$attr_start+$c,1);
	    $c++;
	  }while($chr != "");
		return substr($tag,$attr_start+strlen($attr),$c+1);
	}
*/
	
	
  //-------------------------- VALIDATE
  function valid_email($email){
		return eregi("^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$", $email);
	}
	
	function validate_entries($entry1,$entry2){
		if($entry1 === $entry2){ return true; }else{ return false; }
	}
 //-------------------------- 
 
 //-------------------------- Classes
 function create_function_response($error,$reason){
		$response = array( "error"=>$error, "reason"=>$reason );
		return $response;
 }
  
 //--------------------------  
 
 function generatePassword($length=9, $strength=0){
	$vowels = 'aeiouy';
	$consonants = 'bdghjmnpqrstvz';
	if($strength & 1) { $consonants .= 'BDGHJLMNPQRSTVWXZ'; }
	if($strength & 2) { $vowels .= "AEIOUY"; }
	if($strength & 4) { $consonants .= '23456789'; }
	if($strength & 8) { $consonants .= '@#$%'; }
 
	$password = '';
	$alt = time() % 2;
	for ($i = 0; $i < $length; $i++) {
		if ($alt == 1) {
			$password .= $consonants[(rand() % strlen($consonants))];
			$alt = 0;
		} else {
			$password .= $vowels[(rand() % strlen($vowels))];
			$alt = 1;
		}
	}
	return $password;
	}
 
}


?>