<?php

class cdate {

  // function get_yesterday(){return mktime(0,0,0,date("m"),date("d")-1,date("Y"));}
  // function get_tommorow(){ return mktime(0,0,0,date("m"),date("d")+1,date("Y"));}
  function timestamp(){               return date("Y-m-d G:i:s");} // 2010-01-02 12:13:45
  function date_str(){                return date("Y-m-d");}       // 2010-01-02
  function time_str(){                return date("G:i:s");}       // 12:13:45
  function time_hour(){               return date("G");}           // 12
  function time_min(){                return date("i");}           // 13
  function leap_year(){               return date("L");}           // 1 - 0            !!!!!!!!!!!!!! this leap year only works for the now() year.
  function year_int(){                return date("Y");}           // 2010
  function month_full_str(){          return date("F");}           // January
  function month_part_str(){          return date("F");}           // Jan
  function month_int(){               return date("m");}           // 01
  function day_int(){                 return date("d");}           // 02
  function day_full_str(){            return date("l");}           // Thursday
  function day_of_the_week(){         return date("w");}           // 0 - 6
  function day_part_str(){            return date("D");}           // Thr
  function day_label(){               return date("jS");}          // 5th / 2nd
  function day_of_the_year(){         return date("z");}           // 1 - 365/366
  function nmr_day_in_current_month(){return date("t");}           // 28
  function weekint(){                 return date("W");}           // 1 - 52
  
  function month_list(){
    $months = array("January","February","March","April","May","June","July","August","September","October","November","December");
    return $months;
  }
  
  function short_month_list(){
    $short_months = array("Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec");
    return $short_months;
  }
  
  function month_nmr_of_days_list($year){
  	$intyear = (int) $year;
    if($this->is_year_leap($intyear)){$feb = "29";}
    else{                             $feb = "28";}
    $months_days_list = array("31",$feb,"31","30","31","30","31","31","30","31","30","31");
    return $months_days_list;
  }
  
  function day_list(){
    $days = array("Monday","Tuesday","Thursday","Wednesday","Thursday","Friday","Saterday","Sunday");
    return $days;
  }

	function is_year_leap($year){
    if (!is_int($year) || $year < 0){ return false; }
    if ($year % 4 != 0){
    	return false;//28;
    }else{
    	if ($year % 100 != 0){
      	return true;//29;
      }else{
      	if ($year % 400 != 0){ return false;}//28;}
      	else{                  return true;}//29;}
      }
    }
	}

}
?>