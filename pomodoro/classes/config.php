<?php
ini_set('session.gc_maxlifetime',1*60*5);
session_start();

putenv("TZ=Africa/Johannesburg");
putenv("date.timezone=Africa/Johannesburg");

require('class_checker.php');

define("UNDER_CONSTRUCTION","0");

define("DBUSER",'root');
define("DBPASS",'root');
define("DBNAME",'pomodoro');
define("DBHOST",'localhost');

//define("ACTIVE_URL",'http://macbook-rp.local:8888/Sites/utils/ussd/app');
define("ACTIVE_URL",'https://localhost/code/ussd/app');

define("LOCAL_PATH","/Users/rp/code/ussd/");
define("ONLINE_PATH","");
define("ACTIVE_PATH",constant("LOCAL_PATH"));

define('INCLUDE_FOLDER',"/");
define('JS_DIR','../js/');
define('CSS_DIR','../css/');
define('IMG_DIR','../img/');

// local
define("NAV","");

// production
//define("NAV_PREFIX","/app/");

require("ez_sql_core.php");
require("ez_sql_mysql.php");
require('cdbcon.php');
require('common.php');
require('userdetails.php');
require('simple_html_dom.php');

$current_page = common::get_currecnt_page();
require("check_session.php");
?>