function toggle_timer(){
  $("timer_tbl").style.display = '';
  $("stats_tbl").style.display = 'none';
  $("graphs_tbl").style.display = 'none';
}

function toggle_stats(){
  $("timer_tbl").style.display = 'none';
  $("stats_tbl").style.display = '';
  $("graphs_tbl").style.display = 'none';  
}

function toggle_graphs(){
  $("timer_tbl").style.display = 'none';
  $("stats_tbl").style.display = 'none';
  $("graphs_tbl").style.display = '';
}