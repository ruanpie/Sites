<?php
require("../classes/config.php");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>Pomodoro</title>
    <link href="../css/main.css" rel="stylesheet" />
    <link rel="shortcut icon" href="../img/favicon.ico" />
		<script type="text/javascript">
			window.addEvent('domready',
				function(){ 
					check_timer();
				}
			);
		</script>
    <script type="text/javascript" src="../js/toggle.js" ></script>
    <script type="text/javascript" src="../js/mootools-1.2.4-core-nc.js" ></script>
		<script type="text/javascript" src="../js/g.js" ></script>
    <script type="text/javascript" src="../js/timer.js" ></script>
  </head>
  <body>
    <table align="center">
      <tr>
        <td><?php require('../logout.php'); ?></td>
        <td><button onclick="Javascript:toggle_timer();">Timer</button></td>
        <td><button onclick="Javascript:toggle_stats();">Stats</button></td>
        <td><button onclick="Javascript:toggle_graphs();">Graphs</button></td>
      </tr>
    </table>
    <?php require("pages/timer.php") ?>
    <?php require("pages/stats.php"); ?>
    <?php require("pages/graphs.php"); ?>
    <input type="hidden" id="seconds" />
    <input type="hidden" id="reason" />
  </body>
</html>