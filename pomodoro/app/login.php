<?php
require('../classes/config.php');
require('../classes/log.php');

// account activation:
if(isset($_GET['udid']) && isset($_GET['v'])){
	$a->verify_code($_GET['udid'],$_GET['v']);
}
?>
<html>
<head>
	<title>Pomodoro</title>
  <link href="../css/main.css" rel="stylesheet" />
	<link rel="shortcut icon" href="../img/favicon.ico" />
</head>
<body>
	<form id="frm" method="post" action="login.php" >
    <table width="5%" align="center" style="padding-top:100px;">
      <tr>
        <td>
      		<div class="shadowdiv">
      			<table class="login_tbl">
      				<tr>
      					<td colspan="2" align="center" ><img src="../img/logo.png" /></td>
      				</tr>
      				<tr>
      				  <td class="input_label">Username</td><td class="input_label">Password</td>
      				</tr>
      				<tr>
      					<td align="center"><input class="input_label" type="text" size="10" name="log_username" /></td><td align="center"><input class="input_label" type="password" size="10" name="log_password" /></td>
      				</tr>
      				<tr>
      					<td colspan="2" align="center" ><button type="submit">Login</button></td>
      				</tr>
      			</table>
      		</div>
        </td>
      </tr>		
		</table>
		
	</form>
</body>
</html>