<?php
require('../classes/config.php');

require_once('../calendar/classes/tc_calendar.php');

$pd = new photodiary();

$tabnr = common::handle_empty_post('r',0);
$upload_amount = common::handle_empty_post('upload_sel',1);

require("../../web/page_mods/upload_mod_core.php");

if( isset($uploaded_image_ids) && is_array($uploaded_image_ids) ){
  foreach($uploaded_image_ids as $id){
    $pd->create_photo($id);
  }
  if( $uploaded_image_ids != 0 ){
    common::nav($current_page);
  }
}

require("../inc/header.php");
?>
<form action="<?php print $current_page; ?>" id="img_upl_frm" name="img_upl_frm" method="post" enctype="multipart/form-data">
  <!--<p>
    Album
  </p>
  <p>
    <select></select>
  </p>-->
  <p>
    Date
  </p>
  <p>
    <?php
    $date3_default = common::handle_empty_post(@$_POST['upload_date'],cdate::date_str());
    $myCalendar = new tc_calendar("upload_date", true, false);
	  $myCalendar->setIcon("../calendar/images/iconCalendar.gif");
	  $myCalendar->setDate(date('d', strtotime($date3_default)), date('m', strtotime($date3_default)), date('Y', strtotime($date3_default)));
	  $myCalendar->setPath("../calendar/");
	  $myCalendar->setYearInterval(1970, 2020);
	  //$myCalendar->setAlignment('left', 'center');
	  //$myCalendar->setDatePair('date_from', 'date_to', $date4_default);
	  $myCalendar->writeScript();	  
    ?>
    <br/>
  </p>
  <p>
    Files
  </p>
  <p>
    <select name="upload_sel" id="upload_sel" onchange="$('img_upl_frm').submit();">
    <?php
    
      if(1 === $upload_amount){
        $sel = ' selected="selected" ';
      }else{
        $sel = '';
      }
      print $r->ce("option",'value="1" |'.$sel.' ',"1");
      
      for($a = 1; $a < 5; $a++){
        $btn_amount = $a*5;
        if($btn_amount == $upload_amount){
          $sel = ' selected="selected" ';
        }else{
          $sel = '';
        }
        print $r->ce("option",'value="'.$btn_amount.'" |'.$sel.' ',$btn_amount);
      }
      
    ?>
    </select>
  </p>
  <p id="uploads_inputs">
    <?php $cfile->generate_upload_inputs($upload_amount,$r); ?>
  </p>
  <p>
    <button class="btn" type="submit" onclick="upl();" >Upload</button>
  </p>
  <input type="hidden" name="type" value="img" />
  <input type="hidden" name="r" value="<?php print $tabnr; ?>" />
</form>



<?php
require('../inc/footer.php');
?>