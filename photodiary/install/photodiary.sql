CREATE TABLE `photo_album` (
  `id` int(9) NOT NULL AUTO_INCREMENT,
  `Name` varchar(255) NOT NULL,
  `Description` varchar(255),
  `CoverPhotoId` int(9),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 ;

CREATE TABLE `photo_album_photo` (
  `id` int(9) NOT NULL AUTO_INCREMENT,
  `PhotoAlbumId` int(9) NOT NULL,
  `PhotoId` int(9) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `PhotoAlbumId` (`PhotoAlbumId`),
  KEY `PhotoId` (`PhotoId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ;

CREATE TABLE `photo` (
  `id` int(9) NOT NULL AUTO_INCREMENT,
  `FileId` int(9) NOT NULL,
  `UserId` int(9) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FileId` (`FileId`),
  KEY `UserId` (`UserId`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 ;

ALTER TABLE `photo_album_photo`
  ADD CONSTRAINT `photo_album_ibfk_2` FOREIGN KEY (`PhotoAlbumId`) REFERENCES `photo_album` (`id`),
  ADD CONSTRAINT `photo_id_ibfk_2` FOREIGN KEY (`PhotoId`) REFERENCES `photo` (`id`);
  
ALTER TABLE `photo`
  ADD CONSTRAINT `photo_ibfk_2` FOREIGN KEY (`FileId`) REFERENCES `file` (`FileId`),
  ADD CONSTRAINT `photo_user_ibfk_2` FOREIGN KEY (`UserId`) REFERENCES `user` (`UserId`);
