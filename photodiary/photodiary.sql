-- phpMyAdmin SQL Dump
-- version 3.3.9.2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Mar 13, 2012 at 02:23 PM
-- Server version: 5.5.9
-- PHP Version: 5.3.6

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `photodiary`
--

-- --------------------------------------------------------

--
-- Table structure for table `background`
--

CREATE TABLE `background` (
  `BackgroundId` int(9) NOT NULL AUTO_INCREMENT,
  `FileId` int(9) NOT NULL,
  PRIMARY KEY (`BackgroundId`),
  KEY `fileid` (`FileId`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `background`
--

INSERT INTO `background` VALUES(1, 1);
INSERT INTO `background` VALUES(2, 2);

-- --------------------------------------------------------

--
-- Table structure for table `calendar_activities`
--

CREATE TABLE `calendar_activities` (
  `ActivityId` int(9) NOT NULL AUTO_INCREMENT,
  `Activity` text NOT NULL,
  `Status` varchar(255) NOT NULL,
  `Timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `UserId` int(9) NOT NULL,
  PRIMARY KEY (`ActivityId`),
  KEY `userid` (`UserId`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `calendar_activities`
--

INSERT INTO `calendar_activities` VALUES(2, 'l', '0', '2012-01-13 08:00:00', 1);

-- --------------------------------------------------------

--
-- Table structure for table `chat`
--

CREATE TABLE `chat` (
  `ChatId` int(9) NOT NULL AUTO_INCREMENT,
  `UserId` int(9) NOT NULL,
  `Online` int(1) NOT NULL,
  PRIMARY KEY (`ChatId`),
  KEY `userid` (`UserId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `chat`
--


-- --------------------------------------------------------

--
-- Table structure for table `chat_message`
--

CREATE TABLE `chat_message` (
  `ChatMessageId` int(9) NOT NULL AUTO_INCREMENT,
  `ChatId` int(9) NOT NULL,
  `Message` text NOT NULL,
  PRIMARY KEY (`ChatMessageId`),
  KEY `chatid` (`ChatId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `chat_message`
--


-- --------------------------------------------------------

--
-- Table structure for table `file`
--

CREATE TABLE `file` (
  `FileId` int(9) NOT NULL AUTO_INCREMENT,
  `Name` varchar(244) NOT NULL,
  `Type` varchar(255) NOT NULL,
  `UploadDate` date NOT NULL,
  `ScopeId` int(9) NOT NULL,
  PRIMARY KEY (`FileId`),
  KEY `ScopeId` (`ScopeId`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=51 ;

--
-- Dumping data for table `file`
--

INSERT INTO `file` VALUES(1, 'background.jpg', 'image/jpeg', '2008-11-16', 3);
INSERT INTO `file` VALUES(2, 'DefaultAvatar.png', 'image/png', '2010-06-21', 3);
INSERT INTO `file` VALUES(3, 'admin_banner.gif', 'image/gif', '2010-01-01', 3);
INSERT INTO `file` VALUES(4, 'home_banner.gif', 'image/gif', '2010-01-01', 3);
INSERT INTO `file` VALUES(5, 'content_banner.gif', 'image/gif', '2010-01-01', 3);
INSERT INTO `file` VALUES(6, 'system_settings.gif', 'image/gif', '2010-01-01', 3);
INSERT INTO `file` VALUES(7, 'personalize_banner.gif', 'image/gif', '2010-01-01', 3);
INSERT INTO `file` VALUES(8, 'file_upload_icon.jpg', 'image/jpeg', '2010-01-01', 3);
INSERT INTO `file` VALUES(26, '0ee98e44-5297-41a9-85e4-4d481b8548e9.jpg', 'image/jpeg', '2012-02-28', 1);
INSERT INTO `file` VALUES(27, '6a00d83452cd1869e20147e2d6c5b5970b-800wi.jpg', 'image/jpeg', '2012-02-29', 1);
INSERT INTO `file` VALUES(28, '225225_10150191351957983_761487982_7226594_8377708_n.jpg', 'image/jpeg', '2012-03-01', 1);
INSERT INTO `file` VALUES(29, 'starting point logo.jpg', 'image/jpeg', '2012-03-01', 1);
INSERT INTO `file` VALUES(30, '0cad33761194426576cf3f0c39866e4e2c6ecbd2_m.gif', 'image/gif', '2012-03-01', 1);
INSERT INTO `file` VALUES(31, '0ee98e44-5297-41a9-85e4-4d481b8548e9.jpg', 'image/jpeg', '2012-03-01', 1);
INSERT INTO `file` VALUES(32, '01.Cobra-11-s1.jpg', 'image/jpeg', '2012-03-01', 1);
INSERT INTO `file` VALUES(33, '1.jpg', 'image/jpeg', '2012-03-01', 1);
INSERT INTO `file` VALUES(34, '4ce7d4fd-3632-40ca-936d-69dcb7bd58e3.gif', 'image/gif', '2012-03-01', 1);
INSERT INTO `file` VALUES(35, '6.10.9 pattern people 3.jpg', 'image/jpeg', '2012-03-01', 1);
INSERT INTO `file` VALUES(36, '6a00d83452cd1869e20147e2d6c5b5970b-800wi.jpg', 'image/jpeg', '2012-03-01', 1);
INSERT INTO `file` VALUES(37, '6a0120a85dcdae970b0167625041db970b-800wi.png', 'image/png', '2012-03-01', 1);
INSERT INTO `file` VALUES(38, '7d37fb6e-0529-43aa-9e54-5c33067c0485.gif', 'image/gif', '2012-03-01', 1);
INSERT INTO `file` VALUES(39, '7d4224d28aa8c852a3c74f2363191d9489bb88e5_m.jpg', 'image/jpeg', '2012-03-01', 1);
INSERT INTO `file` VALUES(40, '7d5350a845040df09b8c5a3ef2e25227.jpg', 'image/jpeg', '2012-03-01', 1);
INSERT INTO `file` VALUES(41, '9af91aa2-1622-4564-b469-57ab8e52ebfe.jpg', 'image/jpeg', '2012-03-01', 1);
INSERT INTO `file` VALUES(42, '18fb1b4a_d2d1_c562.jpg', 'image/jpeg', '2012-03-01', 1);
INSERT INTO `file` VALUES(43, '019.jpg', 'image/jpeg', '2012-03-01', 1);
INSERT INTO `file` VALUES(44, '020-05-DSC04859.jpg', 'image/jpeg', '2012-03-01', 1);
INSERT INTO `file` VALUES(45, '50-Hottest-Underboob-Photos-of-All-Time.jpg', 'image/jpeg', '2012-03-01', 1);
INSERT INTO `file` VALUES(46, '165.jpg', 'image/jpeg', '2012-03-01', 1);
INSERT INTO `file` VALUES(47, '191-thinking-of-you-.jpg', 'image/jpeg', '2012-03-01', 1);
INSERT INTO `file` VALUES(48, '4504_84801517982_761487982_1973957_5047232_n.jpg', 'image/jpeg', '2012-03-01', 1);
INSERT INTO `file` VALUES(49, '216493_10150158615152983_761487982_6926159_1570063_n.jpg', 'image/jpeg', '2012-03-01', 1);
INSERT INTO `file` VALUES(50, 'want-vs-need-0.jpeg', 'image/jpeg', '2012-03-02', 1);

-- --------------------------------------------------------

--
-- Table structure for table `group`
--

CREATE TABLE `group` (
  `GroupId` int(9) NOT NULL AUTO_INCREMENT,
  `Group` varchar(50) NOT NULL,
  PRIMARY KEY (`GroupId`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `group`
--

INSERT INTO `group` VALUES(1, 'Administrator');
INSERT INTO `group` VALUES(2, 'User');

-- --------------------------------------------------------

--
-- Table structure for table `group_buttons`
--

CREATE TABLE `group_buttons` (
  `GroupButtonId` int(9) NOT NULL AUTO_INCREMENT,
  `GroupId` int(9) NOT NULL,
  `Menu_ButtonId` int(9) NOT NULL,
  PRIMARY KEY (`GroupButtonId`),
  KEY `menubuttonid` (`Menu_ButtonId`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `group_buttons`
--

INSERT INTO `group_buttons` VALUES(1, 1, 1);
INSERT INTO `group_buttons` VALUES(2, 1, 2);
INSERT INTO `group_buttons` VALUES(3, 1, 3);
INSERT INTO `group_buttons` VALUES(4, 2, 2);
INSERT INTO `group_buttons` VALUES(5, 2, 3);

-- --------------------------------------------------------

--
-- Table structure for table `group_page`
--

CREATE TABLE `group_page` (
  `GroupPageId` int(9) NOT NULL AUTO_INCREMENT,
  `GroupId` int(9) NOT NULL,
  `PageId` int(9) NOT NULL,
  PRIMARY KEY (`GroupPageId`),
  KEY `groupid` (`GroupId`),
  KEY `userid` (`PageId`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=32 ;

--
-- Dumping data for table `group_page`
--

INSERT INTO `group_page` VALUES(1, 1, 1);
INSERT INTO `group_page` VALUES(2, 1, 2);
INSERT INTO `group_page` VALUES(3, 1, 3);
INSERT INTO `group_page` VALUES(4, 1, 4);
INSERT INTO `group_page` VALUES(5, 1, 5);
INSERT INTO `group_page` VALUES(6, 1, 6);
INSERT INTO `group_page` VALUES(7, 1, 7);
INSERT INTO `group_page` VALUES(8, 1, 8);
INSERT INTO `group_page` VALUES(9, 1, 9);
INSERT INTO `group_page` VALUES(10, 1, 10);
INSERT INTO `group_page` VALUES(11, 1, 11);
INSERT INTO `group_page` VALUES(12, 1, 12);
INSERT INTO `group_page` VALUES(13, 1, 13);
INSERT INTO `group_page` VALUES(14, 1, 14);
INSERT INTO `group_page` VALUES(15, 1, 15);
INSERT INTO `group_page` VALUES(16, 1, 16);
INSERT INTO `group_page` VALUES(17, 1, 17);
INSERT INTO `group_page` VALUES(18, 1, 18);
INSERT INTO `group_page` VALUES(19, 1, 19);
INSERT INTO `group_page` VALUES(20, 2, 9);
INSERT INTO `group_page` VALUES(21, 2, 8);
INSERT INTO `group_page` VALUES(22, 2, 10);
INSERT INTO `group_page` VALUES(23, 2, 11);
INSERT INTO `group_page` VALUES(27, 2, 15);
INSERT INTO `group_page` VALUES(29, 2, 17);
INSERT INTO `group_page` VALUES(30, 2, 18);
INSERT INTO `group_page` VALUES(31, 2, 19);

-- --------------------------------------------------------

--
-- Table structure for table `menu_buttons`
--

CREATE TABLE `menu_buttons` (
  `Menu_ButtonId` int(9) NOT NULL AUTO_INCREMENT,
  `root_item` varchar(255) NOT NULL,
  `StatusID` int(9) NOT NULL,
  `button_text` text NOT NULL,
  PRIMARY KEY (`Menu_ButtonId`),
  KEY `btnstatusid` (`StatusID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `menu_buttons`
--

INSERT INTO `menu_buttons` VALUES(1, 'admin', 1, '<td valign="top"><button class="btn" onmouseover="show(''admin'');" onmouseout="hide(''admin'');">Admin</button><br />\n                  <div id="admin" style="display:none; position:absolute; top:15px;" onmouseover="show(''admin'');" onmouseout="hide(''admin'');"><div>&nbsp;</div>\n                  <button class="btn" onclick="load_page(''admin_groups.php'');" >        Groups</button><br />\n                  <button class="btn" onclick="load_page(''admin_buttons.php'');" >       Buttons</button><br />\n                  <button class="btn" onclick="load_page(''admin_users.php'');" >         Users</button><br />\n                  <button class="btn" onclick="load_page(''admin_pages.php'');" >         Pages</button><br />\n                  <button class="btn" onclick="load_page(''admin_themes.php'');" >        Themes</button><br />\n                  <button class="btn" onclick="load_page(''admin_rss.php'');" >           Rss</button><br />\n                  <button class="btn" onclick="load_page(''admin_files.php'');" >         Files</button><br />\n                  <button class="btn" onclick="load_page(''planning.php'');">             planning</button><br />\n                  </div></td>');
INSERT INTO `menu_buttons` VALUES(2, 'content', 1, '<td valign="top"><button class="btn" onmouseover="show(''content'');" onmouseout="hide(''content'');">content</button><br />\r\n									<div id="content" style="display:none; position:absolute; top:15px;" onmouseover="show(''content'');" onmouseout="hide(''content'')" ><div>&nbsp;</div>\r\n									<button class="btn" onclick="load_page(''calendar.php''); " >      calendar</button><br />\r\n									<button class="btn" onclick="load_page(''links.php''); " >         links</button><br />\r\n									<button class="btn" onclick="load_page(''videos.php''); " >        Videos</button><br />\r\n									<button class="btn" onclick="load_page(''music.php''); " >         Music</button><br />\r\n									<button class="btn" onclick="load_page(''rss.php''); " >           rss</button><br/>\r\n									<button class="btn" onclick="load_page(''upload.php'');">          upload</button>\r\n									</div></td>');
INSERT INTO `menu_buttons` VALUES(3, 'settings', 1, '<td valign="top"><button class="btn" onmouseover="show(''settings'');" onmouseout="hide(''settings'');">settings</button><br />\n                  <div id="settings" style="display:none; position:absolute; top:15px;" onmouseover="show(''settings'');" onmouseout="hide(''settings'')" ><div>&nbsp;</div>\n                  <button class="btn" onclick="load_page(''personalize.php'');">personalize</button><br />\n                  <button class="btn" onclick="load_page(''my_settings.php'');">my_settings</button>\n                  </div></td>');

-- --------------------------------------------------------

--
-- Table structure for table `music_player`
--

CREATE TABLE `music_player` (
  `music_player_id` int(9) NOT NULL AUTO_INCREMENT,
  `UserDetailsId` int(9) NOT NULL,
  `alphabetize` enum('0','1') NOT NULL DEFAULT '0',
  `autoload` enum('0','1') NOT NULL DEFAULT '0',
  `autoplay` enum('0','1') NOT NULL DEFAULT '0',
  `repeat` enum('0','1') NOT NULL DEFAULT '0',
  `repeat_playlist` enum('0','1') NOT NULL DEFAULT '0',
  `shuffle` enum('0','1') NOT NULL DEFAULT '0',
  PRIMARY KEY (`music_player_id`),
  KEY `musicuserdetailsid` (`UserDetailsId`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `music_player`
--

INSERT INTO `music_player` VALUES(1, 1, '0', '0', '0', '0', '0', '0');

-- --------------------------------------------------------

--
-- Table structure for table `org_keyword`
--

CREATE TABLE `org_keyword` (
  `Id` int(9) NOT NULL AUTO_INCREMENT,
  `Topic_id` int(9) NOT NULL,
  `Keyword` varchar(100) NOT NULL,
  PRIMARY KEY (`Id`),
  KEY `kywordtopicid` (`Topic_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `org_keyword`
--


-- --------------------------------------------------------

--
-- Table structure for table `org_topic`
--

CREATE TABLE `org_topic` (
  `Id` int(9) NOT NULL AUTO_INCREMENT,
  `ParentTopicId` int(9) DEFAULT NULL,
  `UserId` int(9) NOT NULL,
  `Title` varchar(100) NOT NULL,
  `Body` text,
  `Timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`Id`),
  KEY `userid` (`UserId`),
  KEY `parenttopicid` (`ParentTopicId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `org_topic`
--


-- --------------------------------------------------------

--
-- Table structure for table `page`
--

CREATE TABLE `page` (
  `PageId` int(9) NOT NULL AUTO_INCREMENT,
  `PageName` varchar(255) NOT NULL,
  `StatusID` int(9) NOT NULL,
  PRIMARY KEY (`PageId`),
  UNIQUE KEY `PageName` (`PageName`),
  KEY `pagestatusid` (`StatusID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=20 ;

--
-- Dumping data for table `page`
--

INSERT INTO `page` VALUES(1, 'admin_buttons.php', 1);
INSERT INTO `page` VALUES(2, 'admin_groups.php', 1);
INSERT INTO `page` VALUES(3, 'admin_pages.php', 1);
INSERT INTO `page` VALUES(4, 'admin_users.php', 1);
INSERT INTO `page` VALUES(5, 'admin_themes.php', 1);
INSERT INTO `page` VALUES(6, 'admin_rss.php', 1);
INSERT INTO `page` VALUES(7, 'admin_files.php', 1);
INSERT INTO `page` VALUES(8, 'calendar.php', 1);
INSERT INTO `page` VALUES(9, 'home.php', 1);
INSERT INTO `page` VALUES(10, 'links.php', 1);
INSERT INTO `page` VALUES(11, 'personalize.php', 1);
INSERT INTO `page` VALUES(12, 'planning.php', 1);
INSERT INTO `page` VALUES(13, 'upload.php', 1);
INSERT INTO `page` VALUES(14, 'videos.php', 1);
INSERT INTO `page` VALUES(15, 'organiser.php', 1);
INSERT INTO `page` VALUES(16, 'music.php', 1);
INSERT INTO `page` VALUES(17, 'rss.php', 1);
INSERT INTO `page` VALUES(18, 'app.php', 1);
INSERT INTO `page` VALUES(19, 'my_settings.php', 1);

-- --------------------------------------------------------

--
-- Table structure for table `page_picture`
--

CREATE TABLE `page_picture` (
  `PagePictureId` int(9) NOT NULL AUTO_INCREMENT,
  `PageId` int(9) NOT NULL,
  `FileId` int(9) NOT NULL,
  PRIMARY KEY (`PagePictureId`),
  KEY `PageId` (`PageId`),
  KEY `FileId` (`FileId`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=20 ;

--
-- Dumping data for table `page_picture`
--

INSERT INTO `page_picture` VALUES(1, 1, 3);
INSERT INTO `page_picture` VALUES(2, 2, 3);
INSERT INTO `page_picture` VALUES(3, 3, 3);
INSERT INTO `page_picture` VALUES(4, 4, 3);
INSERT INTO `page_picture` VALUES(5, 5, 3);
INSERT INTO `page_picture` VALUES(6, 6, 3);
INSERT INTO `page_picture` VALUES(7, 7, 3);
INSERT INTO `page_picture` VALUES(8, 8, 5);
INSERT INTO `page_picture` VALUES(9, 9, 4);
INSERT INTO `page_picture` VALUES(10, 10, 5);
INSERT INTO `page_picture` VALUES(11, 11, 7);
INSERT INTO `page_picture` VALUES(12, 12, 3);
INSERT INTO `page_picture` VALUES(13, 13, 5);
INSERT INTO `page_picture` VALUES(14, 14, 5);
INSERT INTO `page_picture` VALUES(15, 15, 5);
INSERT INTO `page_picture` VALUES(16, 16, 5);
INSERT INTO `page_picture` VALUES(17, 17, 5);
INSERT INTO `page_picture` VALUES(18, 18, 1);
INSERT INTO `page_picture` VALUES(19, 19, 6);

-- --------------------------------------------------------

--
-- Table structure for table `photo`
--

CREATE TABLE `photo` (
  `id` int(9) NOT NULL AUTO_INCREMENT,
  `FileId` int(9) NOT NULL,
  `UserId` int(9) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FileId` (`FileId`),
  KEY `UserId` (`UserId`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=26 ;

--
-- Dumping data for table `photo`
--

INSERT INTO `photo` VALUES(1, 26, 1);
INSERT INTO `photo` VALUES(2, 27, 1);
INSERT INTO `photo` VALUES(3, 28, 1);
INSERT INTO `photo` VALUES(4, 29, 1);
INSERT INTO `photo` VALUES(5, 30, 1);
INSERT INTO `photo` VALUES(6, 31, 1);
INSERT INTO `photo` VALUES(7, 32, 1);
INSERT INTO `photo` VALUES(8, 33, 1);
INSERT INTO `photo` VALUES(9, 34, 1);
INSERT INTO `photo` VALUES(10, 35, 1);
INSERT INTO `photo` VALUES(11, 36, 1);
INSERT INTO `photo` VALUES(12, 37, 1);
INSERT INTO `photo` VALUES(13, 38, 1);
INSERT INTO `photo` VALUES(14, 39, 1);
INSERT INTO `photo` VALUES(15, 40, 1);
INSERT INTO `photo` VALUES(16, 41, 1);
INSERT INTO `photo` VALUES(17, 42, 1);
INSERT INTO `photo` VALUES(18, 43, 1);
INSERT INTO `photo` VALUES(19, 44, 1);
INSERT INTO `photo` VALUES(20, 45, 1);
INSERT INTO `photo` VALUES(21, 46, 1);
INSERT INTO `photo` VALUES(22, 47, 1);
INSERT INTO `photo` VALUES(23, 48, 1);
INSERT INTO `photo` VALUES(24, 49, 1);
INSERT INTO `photo` VALUES(25, 50, 1);

-- --------------------------------------------------------

--
-- Table structure for table `photo_album`
--

CREATE TABLE `photo_album` (
  `id` int(9) NOT NULL AUTO_INCREMENT,
  `Name` varchar(255) NOT NULL,
  `Description` varchar(255) DEFAULT NULL,
  `CoverPhotoId` int(9) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `photo_album`
--


-- --------------------------------------------------------

--
-- Table structure for table `photo_album_photo`
--

CREATE TABLE `photo_album_photo` (
  `id` int(9) NOT NULL AUTO_INCREMENT,
  `PhotoAlbumId` int(9) NOT NULL,
  `PhotoId` int(9) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `PhotoAlbumId` (`PhotoAlbumId`),
  KEY `PhotoId` (`PhotoId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `photo_album_photo`
--


-- --------------------------------------------------------

--
-- Table structure for table `planning`
--

CREATE TABLE `planning` (
  `PlanningId` int(9) NOT NULL AUTO_INCREMENT,
  `Description` text NOT NULL,
  `Status` varchar(20) NOT NULL DEFAULT 'False',
  `UserId` int(9) NOT NULL,
  `Timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`PlanningId`),
  KEY `userid` (`UserId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `planning`
--


-- --------------------------------------------------------

--
-- Table structure for table `playlist`
--

CREATE TABLE `playlist` (
  `PlaylistId` int(9) NOT NULL AUTO_INCREMENT,
  `PlaylistURL` text NOT NULL,
  PRIMARY KEY (`PlaylistId`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `playlist`
--

INSERT INTO `playlist` VALUES(1, 'playlist.xspf');
INSERT INTO `playlist` VALUES(2, 'playlist.xspf');
INSERT INTO `playlist` VALUES(3, 'playlist.xspf');
INSERT INTO `playlist` VALUES(4, 'playlist.xspf');
INSERT INTO `playlist` VALUES(5, 'playlist.xspf');

-- --------------------------------------------------------

--
-- Table structure for table `posted_links`
--

CREATE TABLE `posted_links` (
  `linkId` int(11) NOT NULL AUTO_INCREMENT,
  `url` text NOT NULL,
  `UserId` int(9) NOT NULL,
  `Timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`linkId`),
  KEY `userid` (`UserId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `posted_links`
--


-- --------------------------------------------------------

--
-- Table structure for table `rss_feed`
--

CREATE TABLE `rss_feed` (
  `Id` int(9) NOT NULL AUTO_INCREMENT,
  `URL` text NOT NULL,
  `ScopeId` int(9) NOT NULL,
  `last_refresh` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `feed_type` varchar(255) DEFAULT '1',
  PRIMARY KEY (`Id`),
  KEY `ScopeId` (`ScopeId`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `rss_feed`
--

INSERT INTO `rss_feed` VALUES(1, 'http://henrik.nyh.se/scrapers/cyanide_and_happiness.rss', 1, '2012-01-20 13:57:37', '2');

-- --------------------------------------------------------

--
-- Table structure for table `rss_feed_entry`
--

CREATE TABLE `rss_feed_entry` (
  `EntryId` int(9) NOT NULL AUTO_INCREMENT,
  `FeedId` int(9) NOT NULL,
  `channel` text,
  `title` text,
  `link` text,
  `comments` text,
  `pubDate` text,
  `timestamp` text,
  `description` text,
  `isPermaLink` text,
  `creator` text,
  `content` text,
  `commentRss` text,
  PRIMARY KEY (`EntryId`),
  KEY `FeedId` (`FeedId`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=17 ;

--
-- Dumping data for table `rss_feed_entry`
--

INSERT INTO `rss_feed_entry` VALUES(1, 1, NULL, '01.20.2012', NULL, NULL, 'Fri, 20 Jan 2012 00:00:00 +0000', NULL, '<img src="http://www.flashasylum.com/db/files/Comics/Rob/drugs.png" alt="Comic" >', NULL, NULL, NULL, NULL);
INSERT INTO `rss_feed_entry` VALUES(2, 1, NULL, 'We''re back!', NULL, NULL, 'Thu, 19 Jan 2012 00:18:34 +0000', NULL, 'News/Article', NULL, NULL, NULL, NULL);
INSERT INTO `rss_feed_entry` VALUES(3, 1, NULL, '01.19.2012', NULL, NULL, 'Thu, 19 Jan 2012 00:00:00 +0000', NULL, '<img src="http://www.flashasylum.com/db/files/Comics/Dave/comicpushpush1.png" alt="Comic" >', NULL, NULL, NULL, NULL);
INSERT INTO `rss_feed_entry` VALUES(4, 1, NULL, '01.18.2012', NULL, NULL, 'Wed, 18 Jan 2012 00:00:00 +0000', NULL, '<img src="http://flashasylum.com/db/files/Comics/Rob/missed-a-day.jpg" alt="Comic" >', NULL, NULL, NULL, NULL);
INSERT INTO `rss_feed_entry` VALUES(5, 1, NULL, '01.17.2012', NULL, NULL, 'Tue, 17 Jan 2012 00:00:00 +0000', NULL, '<img src="http://www.flashasylum.com/db/files/Comics/Matt/He-died-how-he-lived...-drinking.png" alt="Comic" >', NULL, NULL, NULL, NULL);
INSERT INTO `rss_feed_entry` VALUES(6, 1, NULL, '01.16.2012', NULL, NULL, 'Mon, 16 Jan 2012 00:00:00 +0000', NULL, '<img src="http://www.flashasylum.com/db/files/Comics/Kris/peeve.png" alt="Comic" >', NULL, NULL, NULL, NULL);
INSERT INTO `rss_feed_entry` VALUES(7, 1, NULL, '01.15.2012', NULL, NULL, 'Sun, 15 Jan 2012 00:00:00 +0000', NULL, '<img src="http://www.flashasylum.com/db/files/Comics/Rob/homies.png" alt="Comic" >', NULL, NULL, NULL, NULL);
INSERT INTO `rss_feed_entry` VALUES(8, 1, NULL, '01.14.2012', NULL, NULL, 'Sat, 14 Jan 2012 00:00:00 +0000', NULL, '<img src="http://www.flashasylum.com/db/files/Comics/Dave/comictwodadsnew2.png" alt="Comic" >', NULL, NULL, NULL, NULL);
INSERT INTO `rss_feed_entry` VALUES(9, 1, NULL, '01.13.2012', NULL, NULL, 'Fri, 13 Jan 2012 00:00:00 +0000', NULL, '<img src="http://www.flashasylum.com/db/files/Comics/Kris/experiment.png" alt="Comic" >', NULL, NULL, NULL, NULL);
INSERT INTO `rss_feed_entry` VALUES(10, 1, NULL, '01.12.2012', NULL, NULL, 'Thu, 12 Jan 2012 00:00:00 +0000', NULL, '<img src="http://www.flashasylum.com/db/files/Comics/Rob/9months.png" alt="Comic" >', NULL, NULL, NULL, NULL);
INSERT INTO `rss_feed_entry` VALUES(11, 1, NULL, '01.24.2012', NULL, NULL, 'Tue, 24 Jan 2012 00:00:00 +0000', NULL, '<img src="http://www.flashasylum.com/db/files/Comics/Matt/Dress-for-the-job-you-want...-sometimes..png" alt="Comic" >', NULL, NULL, NULL, NULL);
INSERT INTO `rss_feed_entry` VALUES(12, 1, NULL, 'Delicious News', NULL, NULL, 'Mon, 23 Jan 2012 14:53:00 +0000', NULL, 'News/Article', NULL, NULL, NULL, NULL);
INSERT INTO `rss_feed_entry` VALUES(13, 1, NULL, '01.23.2012', NULL, NULL, 'Mon, 23 Jan 2012 00:00:00 +0000', NULL, '<img src="http://www.flashasylum.com/db/files/Comics/Rob/dragon.png" alt="Comic" >', NULL, NULL, NULL, NULL);
INSERT INTO `rss_feed_entry` VALUES(14, 1, NULL, '01.22.2012', NULL, NULL, 'Sun, 22 Jan 2012 00:00:00 +0000', NULL, '<img src="http://www.flashasylum.com/db/files/Comics/Dave/comictwodadshomework1.png" alt="Comic" >', NULL, NULL, NULL, NULL);
INSERT INTO `rss_feed_entry` VALUES(15, 1, NULL, '01.21.2012', NULL, NULL, 'Sat, 21 Jan 2012 00:00:00 +0000', NULL, '<img src="http://www.flashasylum.com/db/files/Comics/Kris/grave.png" alt="Comic" >', NULL, NULL, NULL, NULL);
INSERT INTO `rss_feed_entry` VALUES(16, 1, NULL, 'New signed print', NULL, NULL, 'Fri, 20 Jan 2012 17:36:01 +0000', NULL, 'News/Article', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `scope`
--

CREATE TABLE `scope` (
  `Id` int(9) NOT NULL AUTO_INCREMENT,
  `Scope` varchar(255) NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `scope`
--

INSERT INTO `scope` VALUES(1, 'public');
INSERT INTO `scope` VALUES(2, 'private');
INSERT INTO `scope` VALUES(3, 'system');
INSERT INTO `scope` VALUES(4, 'rss_feed_picture');
INSERT INTO `scope` VALUES(5, 'SCOPE_FEED_CYANIDE');

-- --------------------------------------------------------

--
-- Table structure for table `status`
--

CREATE TABLE `status` (
  `StatusID` int(9) NOT NULL AUTO_INCREMENT,
  `Status` varchar(100) NOT NULL,
  PRIMARY KEY (`StatusID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `status`
--

INSERT INTO `status` VALUES(1, 'Enabled');
INSERT INTO `status` VALUES(2, 'Disabled');
INSERT INTO `status` VALUES(3, 'Deleted');

-- --------------------------------------------------------

--
-- Table structure for table `theme`
--

CREATE TABLE `theme` (
  `ThemeId` int(9) NOT NULL AUTO_INCREMENT,
  `Theme` text NOT NULL,
  `cssRef` varchar(255) NOT NULL,
  PRIMARY KEY (`ThemeId`),
  UNIQUE KEY `cssRef` (`cssRef`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `theme`
--

INSERT INTO `theme` VALUES(1, 'elements', 'elements.css');
INSERT INTO `theme` VALUES(2, 'funky', 'funky.css');
INSERT INTO `theme` VALUES(3, 'lightblue', 'lightblue.css');
INSERT INTO `theme` VALUES(4, 'Cool', 'Cool.css');
INSERT INTO `theme` VALUES(5, 'Black', 'Black.css');

-- --------------------------------------------------------

--
-- Table structure for table `theme_details`
--

CREATE TABLE `theme_details` (
  `ThemeId` int(9) NOT NULL,
  `detail_1` varchar(20) NOT NULL,
  `detail_2` varchar(20) NOT NULL,
  `detail_3` varchar(20) NOT NULL,
  `detail_4` varchar(20) NOT NULL,
  `detail_5` varchar(20) NOT NULL,
  `detail_6` varchar(20) NOT NULL,
  `detail_7` varchar(20) NOT NULL,
  `detail_8` varchar(20) NOT NULL,
  `detail_9` varchar(20) NOT NULL,
  `detail_10` varchar(20) NOT NULL,
  `detail_11` varchar(20) NOT NULL,
  `detail_12` varchar(20) NOT NULL,
  PRIMARY KEY (`ThemeId`),
  KEY `themeid` (`ThemeId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `theme_details`
--

INSERT INTO `theme_details` VALUES(1, '#FFFFFF', '#000000', '#ababab', '#000000', '#c4c4c4', '#000000', '#FFFFFF', '#000000', '#c4c4c4', '#000000', '#ababab', '#000000');
INSERT INTO `theme_details` VALUES(2, '#FFFFFF', '#f48d08', '#d27624', '#000000', '#f8ab48', '#000000', '#d27624', '#000000', '#f8ab48', '#000000', '#d27624', '#000000');
INSERT INTO `theme_details` VALUES(3, '#FFFFFF', '#33CCFF', '#3366FF', '#000000', '#33CCFF', '#000000', '#33CCFF', '#000000', '#3366FF', '#000000', '#3366FF', '#000000');
INSERT INTO `theme_details` VALUES(4, '#000099', '#3399FF', '#FFCC33', '#000000', '#FF6600', '#000000', '#EEEEEE', '#000000', '#33CCCC', '#000099', '#33CCCC', '#000099');
INSERT INTO `theme_details` VALUES(5, '#000000', '#BBBBBB', '#444444', '#EEEEEE', '#000000', '#BBBBBB', '#000000', '#BBBBBB', '#444444', '#EEEEEE', '#000000', '#BBBBBB');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `UserId` int(9) NOT NULL AUTO_INCREMENT,
  `Username` varchar(50) NOT NULL,
  `Password` varchar(50) NOT NULL,
  `GroupId` int(9) NOT NULL,
  PRIMARY KEY (`UserId`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `user`
--

INSERT INTO `user` VALUES(1, 'admin', '7a2d8a17b063b56ece33d4a5379942c3', 1);

-- --------------------------------------------------------

--
-- Table structure for table `userdetails`
--

CREATE TABLE `userdetails` (
  `UserDetailsId` int(9) NOT NULL AUTO_INCREMENT,
  `Active` varchar(20) NOT NULL,
  `ThemeId` int(9) NOT NULL,
  `BackgroundId` int(9) NOT NULL,
  `UseBackground` varchar(1) NOT NULL,
  `UserId` int(9) NOT NULL,
  `Opacity` int(6) NOT NULL,
  `TrsButtons` varchar(1) NOT NULL,
  `PlaylistId` int(9) NOT NULL,
  `AvatarId` int(9) NOT NULL,
  `Email` varchar(100) NOT NULL,
  `MSISDN` varchar(13) DEFAULT NULL,
  `LastPageId` int(9) DEFAULT NULL,
  `VerificationCode` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`UserDetailsId`),
  KEY `userid` (`UserId`),
  KEY `backgroundid` (`BackgroundId`),
  KEY `playlistid` (`PlaylistId`),
  KEY `AvatarId` (`AvatarId`),
  KEY `LastPageId` (`LastPageId`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `userdetails`
--

INSERT INTO `userdetails` VALUES(1, 'true', 1, 1, '0', 1, 100, '0', 1, 2, 'ruan800@gmail.com', NULL, 4, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `user_rss_feeds`
--

CREATE TABLE `user_rss_feeds` (
  `UserRssFeedId` int(9) NOT NULL AUTO_INCREMENT,
  `FeedId` int(9) NOT NULL,
  `UserId` int(9) NOT NULL,
  PRIMARY KEY (`UserRssFeedId`),
  KEY `FeedId` (`FeedId`),
  KEY `UserId` (`UserId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `user_rss_feeds`
--


--
-- Constraints for dumped tables
--

--
-- Constraints for table `background`
--
ALTER TABLE `background`
  ADD CONSTRAINT `background_ibfk_1` FOREIGN KEY (`FileId`) REFERENCES `file` (`FileId`);

--
-- Constraints for table `calendar_activities`
--
ALTER TABLE `calendar_activities`
  ADD CONSTRAINT `calendar_activities_ibfk_1` FOREIGN KEY (`UserId`) REFERENCES `user` (`UserId`);

--
-- Constraints for table `chat`
--
ALTER TABLE `chat`
  ADD CONSTRAINT `chat_ibfk_1` FOREIGN KEY (`UserId`) REFERENCES `user` (`UserId`);

--
-- Constraints for table `chat_message`
--
ALTER TABLE `chat_message`
  ADD CONSTRAINT `chat_message_ibfk_1` FOREIGN KEY (`ChatId`) REFERENCES `chat` (`ChatId`);

--
-- Constraints for table `file`
--
ALTER TABLE `file`
  ADD CONSTRAINT `file_ibfk_1` FOREIGN KEY (`ScopeId`) REFERENCES `scope` (`Id`);

--
-- Constraints for table `group_buttons`
--
ALTER TABLE `group_buttons`
  ADD CONSTRAINT `group_buttons_ibfk_1` FOREIGN KEY (`Menu_ButtonId`) REFERENCES `menu_buttons` (`Menu_ButtonId`);

--
-- Constraints for table `group_page`
--
ALTER TABLE `group_page`
  ADD CONSTRAINT `group_page_ibfk_1` FOREIGN KEY (`GroupId`) REFERENCES `group` (`GroupId`),
  ADD CONSTRAINT `group_page_ibfk_2` FOREIGN KEY (`PageId`) REFERENCES `page` (`PageId`);

--
-- Constraints for table `menu_buttons`
--
ALTER TABLE `menu_buttons`
  ADD CONSTRAINT `menu_buttons_ibfk_1` FOREIGN KEY (`StatusID`) REFERENCES `status` (`StatusID`);

--
-- Constraints for table `org_keyword`
--
ALTER TABLE `org_keyword`
  ADD CONSTRAINT `org_keyword_ibfk_1` FOREIGN KEY (`Topic_id`) REFERENCES `org_topic` (`Id`);

--
-- Constraints for table `org_topic`
--
ALTER TABLE `org_topic`
  ADD CONSTRAINT `org_topic_ibfk_1` FOREIGN KEY (`UserId`) REFERENCES `user` (`UserId`),
  ADD CONSTRAINT `org_topic_ibfk_2` FOREIGN KEY (`ParentTopicId`) REFERENCES `org_topic` (`Id`);

--
-- Constraints for table `page`
--
ALTER TABLE `page`
  ADD CONSTRAINT `page_ibfk_1` FOREIGN KEY (`StatusID`) REFERENCES `status` (`StatusID`);

--
-- Constraints for table `page_picture`
--
ALTER TABLE `page_picture`
  ADD CONSTRAINT `page_picture_ibfk_1` FOREIGN KEY (`PageId`) REFERENCES `page` (`PageId`),
  ADD CONSTRAINT `page_picture_ibfk_2` FOREIGN KEY (`FileId`) REFERENCES `file` (`FileId`);

--
-- Constraints for table `photo`
--
ALTER TABLE `photo`
  ADD CONSTRAINT `photo_ibfk_2` FOREIGN KEY (`FileId`) REFERENCES `file` (`FileId`),
  ADD CONSTRAINT `photo_user_ibfk_2` FOREIGN KEY (`UserId`) REFERENCES `user` (`UserId`);

--
-- Constraints for table `photo_album_photo`
--
ALTER TABLE `photo_album_photo`
  ADD CONSTRAINT `photo_album_ibfk_2` FOREIGN KEY (`PhotoAlbumId`) REFERENCES `photo_album` (`id`),
  ADD CONSTRAINT `photo_id_ibfk_2` FOREIGN KEY (`PhotoId`) REFERENCES `photo` (`id`);

--
-- Constraints for table `planning`
--
ALTER TABLE `planning`
  ADD CONSTRAINT `planning_ibfk_1` FOREIGN KEY (`UserId`) REFERENCES `user` (`UserId`);

--
-- Constraints for table `posted_links`
--
ALTER TABLE `posted_links`
  ADD CONSTRAINT `posted_links_ibfk_1` FOREIGN KEY (`UserId`) REFERENCES `user` (`UserId`);

--
-- Constraints for table `rss_feed`
--
ALTER TABLE `rss_feed`
  ADD CONSTRAINT `rss_feed_ibfk_1` FOREIGN KEY (`ScopeId`) REFERENCES `scope` (`Id`);

--
-- Constraints for table `rss_feed_entry`
--
ALTER TABLE `rss_feed_entry`
  ADD CONSTRAINT `rss_feed_entry_ibfk_1` FOREIGN KEY (`FeedId`) REFERENCES `rss_feed` (`Id`);

--
-- Constraints for table `theme_details`
--
ALTER TABLE `theme_details`
  ADD CONSTRAINT `theme_details_ibfk_1` FOREIGN KEY (`ThemeId`) REFERENCES `theme` (`ThemeId`);

--
-- Constraints for table `userdetails`
--
ALTER TABLE `userdetails`
  ADD CONSTRAINT `userdetails_ibfk_1` FOREIGN KEY (`UserId`) REFERENCES `user` (`UserId`),
  ADD CONSTRAINT `userdetails_ibfk_2` FOREIGN KEY (`BackgroundId`) REFERENCES `background` (`BackgroundId`),
  ADD CONSTRAINT `userdetails_ibfk_3` FOREIGN KEY (`PlaylistId`) REFERENCES `playlist` (`PlaylistId`),
  ADD CONSTRAINT `userdetails_ibfk_4` FOREIGN KEY (`AvatarId`) REFERENCES `file` (`FileId`),
  ADD CONSTRAINT `userdetails_ibfk_5` FOREIGN KEY (`LastPageId`) REFERENCES `page` (`PageId`);

--
-- Constraints for table `user_rss_feeds`
--
ALTER TABLE `user_rss_feeds`
  ADD CONSTRAINT `user_rss_feeds_ibfk_1` FOREIGN KEY (`FeedId`) REFERENCES `rss_feed` (`Id`),
  ADD CONSTRAINT `user_rss_feeds_ibfk_2` FOREIGN KEY (`UserId`) REFERENCES `user` (`UserId`);
