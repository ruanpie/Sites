<?php
check_dependancy("common.php");
check_dependancy("schema.php");
check_dependancy("pages.php");

if(schema::is_database_valid(constant('DBNAME'))){ $installed = true;
}else{                                             $installed = false; }

/*
if($current_page == "login.php" || $current_page == "register.php" || $current_page == "install.php" ){
}else{
  if(isset($_SESSION['ui'])){
		$page_details = $p->open_group_page($_SESSION['user_type_id'],$current_page);
		if(!(@$page_details->PageId)){
			$lastpage = userdetails::get_last_page_by_userid($_SESSION['ui']);
			access_denied_page($lastpage);
		}else{
			$_SESSION['lastpage'] = $current_page;
			$current_page_id = pages::get_page_id_from_name($current_page);
			if(    $page_details->StatusId == constant('ENABLED')){  userdetails::set_user_last_page($current_page_id,$_SESSION['ui']); }
			elseif($page_details->StatusId == constant('DISABLED')){ page_disabled_page($current_page); }
		}
  }
}
*/

function access_denied_page($lastPage){
	//die("<html><body style=\"background-color:#FF0033;\">Access denied<br/>go back to <a href=\"".$lastPage."\">".$lastPage."</a></body></html>");
}

function page_disabled_page($lastPage){
	die("<html><body style=\"background-color:#FF0033;\">Page is disabled<br/>go back to <a href=\"".$lastPage."\">".$lastPage."</a></body></html>");
}

class check_session extends common{
  public $current_page;

  function check_session($current_page){
  	$dbclass = new cdbcon();
  	$db = $dbclass->return_db_con();
    if(isset($_SESSION['ui']) && isset($_SESSION['user_type_id'])){
      if($current_page == "login.php"){
      	common::nav('app.php');
      }
    }else{
      switch ($current_page){
        case "login.php"   : break;
        case "register.php": break;
        case "install.php" : break; // this should be more secure
        default            : common::nav("login.php"); break;
      }
    }

  }

}
$chk = new check_session($current_page);
?>