<?php
check_dependancy('render.php');
check_dependancy('dbif.php');
define('BUTTON_MAX',10);

class dbhtml {

	function entries_html_rows($sql_table, $column_html_array, $action_html_array, $table_offset, $table_limit, $sql_clause){
		
		$entries_array = dbif::listall_offset_clause($sql_table,$table_offset,$table_limit,$sql_clause);
	
		$column_array = dbif::table_columns($sql_table);
		$html = '';
		foreach($column_array as $column){
				$html .= render::ce("td","",render::ce("b","",$column));
			}
			
			$html .= $this->create_tds($column_html_array,0,$table_offset);
			$html = render::ce("tr","",$html);
			
			if(is_array($entries_array)){ // Rows / Entries
				$c=1;
				foreach($entries_array as $entry){	
					$column_entry_html = "";
					foreach($column_array as $column){
						$column_entry_html .= render::ce("td","title='".$column."'",$entry->$column);
					}
					$column_entry_html .= $this->create_tds_selfclosing_elem($action_html_array,$c,$table_offset);
					$html .= render::ce("tr","id='tr_".($c+$table_offset)."'",$column_entry_html);
					$c++;
				}
			}
		return $html;
	}

	function entries_html($argument_array){	

		$html = '';
		$table_attributes  = $argument_array["table_attributes"];    // html table attributes
		$column_html_array = $argument_array["column_array"];        // array containing the Column html
		$action_html_array = $argument_array["action_array"];        // all the available actions
		$table_limit       = $argument_array["table_limit"];         // limit per table
		$sql_table         = $argument_array["sql_table"];           // sql table/s quering from
		$table_page        = $argument_array["table_page"];          // the current page where the table will be displayed
		$get               = $argument_array["app_get_urlr"];        // $GET
		$sql_clause        = $argument_array["query_addition"];      // Extra SQL where clause
		
		// TODO FIND A WAY TO LOOK FOR PARAMETERS, but the above should not break if parameter in array is not present 
		
		 // TODO abstract simmilar behavior
		if(isset($get['o']) && $get['o'] > 0){
			$table_offset = $get['o'];
		}else{
			$table_offset = 0;
		}
		
		if(isset($get['bo']) && $get['bo']){
			$button_offset = $get['bo'];
		}else{
			$button_offset = 0;
		}
			
		$column_array = dbif::table_columns($sql_table);
		$table_entries_count = dbif::table_count_clause($sql_table,$sql_clause);
		$colspan = count($column_array) + count($action_html_array);
		
		
		$html = $this->entries_html_rows($sql_table, $column_html_array, $action_html_array, $table_offset, $table_limit, $sql_clause);
		
		
		$a = explode(".",( ($table_entries_count -1) / $table_limit)); // -1 to prevent it from creating the last button, but the last button will not have any records
		$btn_amount = $a[0]; // TOTAL AMOUNT OF BUTTNONS for all the records of that table
		$next_html = "";
		if($btn_amount > constant('BUTTON_MAX') && ($button_offset + constant('BUTTON_MAX')) < $btn_amount){
			$next_offset = $button_offset * constant('BUTTON_MAX');
			$next_html = render::ce("button","class='btn'|onclick='location.href = ^".$table_page."?l=".$table_limit."&o=".$next_offset."&bo=".($button_offset + constant('BUTTON_MAX'))."^' ","Next");	
		}
	
		$prev_html = "";
		if($button_offset + constant('BUTTON_MAX') > constant('BUTTON_MAX')){
			$next_offset = $button_offset * constant('BUTTON_MAX');
			$prev_html = render::ce("button","class='btn'|onclick='location.href = ^".$table_page."?l=".$table_limit."&o=".$next_offset."&bo=".($button_offset - constant('BUTTON_MAX'))."^' ","Prev");	
		}			
	
	  // BUTONS
	  $results_table_id = $sql_table."_tbl";
		$null_offset = ($table_limit*0);
		$buttons = render::ce("button","class='btn'|onclick='next_results(^".$results_table_id."^,^".$table_page."^,".$table_limit.",".$null_offset.");' ",0);
		for($b = $button_offset; $b < $btn_amount+$button_offset; $b++){
			$break_point = 0;
			if($button_offset == 0){
				$break_point = constant('BUTTON_MAX');
			}else{
				$break_point = $button_offset +constant('BUTTON_MAX');
			}
			if($b == $break_point) break;
			//$buttons .= render::ce("button","onclick='location.href = ^".$table_page."?l=".$table_limit."&o=".."^' ",($b+1));
			$next_offset = ($table_limit*($b+1));
			$buttons .= render::ce("button","class='btn'|onclick='next_results(^".$results_table_id."^,^".$table_page."^,".$table_limit.",".$next_offset.");' ",($b+1));	
		}
		$nav_btns = render::ce("table","",render::ce("tr","",render::ce("td","colspan='".$colspan."'|align='center'",$prev_html.$buttons.$next_html)));
		
		
		return render::ce("table",$table_attributes." id='".$results_table_id."' ",$html.$nav_btns);
	}
	
	// TODO columns array, value array, AND $GET, take all the values not mentioned in columns array and value array, and put them into the final URL555
	function construct_get_url(){ }

	function create_tds($elem_array,$c,$table_offset){
		$html = '';
		foreach($elem_array as $el){
			$row_nmr = ($c + $table_offset);
			$complete_attr = str_ireplace("()","(".$row_nmr.")",$el['attr']);
			$html .= render::ce("td","",render::ce($el['elem'], $complete_attr, $el['content']));
		}
		return $html;
	}
	
	function create_tds_selfclosing_elem($elem_array,$c,$table_offset){
		$html = '';
		foreach($elem_array as $el){
			$row_nmr = ($c + $table_offset);
			$complete_attr = str_ireplace("()","(".$row_nmr.")",$el['attr']);
			$html .= render::ce("td","",render::cse($el['elem'], $complete_attr));
		}
		return $html;
	}
	
	function action_buttons($html,$action_array){
		$html = '';
		foreach($action_array as $act){
			switch($act){
				case "edit":
					$html .= render::ce("td","",render::cse("img","src='img/edit.png'"));
					break;
				case "delete":
					$html .= render::ce("td","",render::cse("img","src='img/del.gif'"));
					break;
				default:
					break;
			}
		}
		return $html;
	}
	
	function all_dbs_html(){
		$databases = dbif::listall_databases();
		$database_html = '';
		foreach($databases as $d){
			if($d->Database != ''){
			$database_html .= render::ce("option","",$d->Database);
			}
		}
		return $database_html;
	}

	
}

?>