<?php
check_dependancy("common.php");
check_dependancy("cdate.php");
check_dependancy("dbhtml.php");
check_dependancy("dbif.php");
check_dependancy("render.php");

class ccalendar extends common {
	private $year;
	private $date;
	static $dbcon;
	
	function ccalendar(){
		$dbclass = new cdbcon();
  	$db = $dbclass->return_db_con();
  	ccalendar::$dbcon = $db;
	}
	
	function add_activity($get,$UI){
		if(isset($get)){
			$months = cdate::month_list();
	  	$month_int = (array_search($get['month'],$months)+1);
	  	$Timestamp = $get['year']."-".$month_int."-".$get['day']." ".$get['hr'].":".$get['min'].":00";
	  	ccalendar::$dbcon->query("INSERT INTO `calendar_activities` (`Activity`,`Status`,`UserId`,`Timestamp`)VALUES ('".$get['act']."','0','".$UI."','".$Timestamp."') ");
	  	return true;
	  }
	}
	
	function del_act($actid){
		ccalendar::$dbcon->query(" DELETE FROM `calendar_activities` WHERE `ActivityId` = '".$actid."' ");
		return true;
	}
	
	function get_today_acts(){
		$date_array = explode("-",cdate::date_str());
		$year  = $date_array[0];
		$month = $date_array[1];
		$day   = $date_array[2];
		return $this->get_activities($year,$month,$day,$_SESSION['ui']);
	}
	
	function get_activities($year,$month,$day,$UI){
		$all_acts = ccalendar::$dbcon->get_results("SELECT * FROM `calendar_activities` WHERE UserId = '".$UI."' AND DATE(`Timestamp`) = '".$year."-".$month."-".$day."' ORDER BY `Timestamp` ");
		return $all_acts;
	}
	
	function get_all_acts_html($year_int,$month_int,$day_int,$UI){
		$edit_c = array('elem'=>"img", 'attr'=>'', 'content'=>render::ce("b","","Edit"));
		$del_c  = array('elem'=>"img", 'attr'=>'', 'content'=>render::ce("b","","Delete"));
		
		$edit_a = array('elem'=>"img", 'attr'=>'src="../img/edit.png" onClick="edit_row();" class="click_img" ');
		$del_a  = array('elem'=>"img", 'attr'=>'src="../img/del.gif" onClick="del_row();" class="click_img" ');
		
		$argument_array = array("table_attributes"=> "border='1'|style='border-collapse:collapse;'|cellpadding=8px|cellspacing=8px|align='center'",
														"column_array"    => array($edit_c,$del_c),
		                        "action_array"    => array($edit_a,$del_a),
		                        "table_limit"     => 6,
		                        "sql_table"       => 'calendar_activities',
		                        "table_page"      => 'app.php',//$current_page,
		                        "app_get_urlr"    => $_GET,
		                        "query_addition"  => " WHERE UserId = '".$UI."' AND DATE(`Timestamp`) = '".$year_int."-".$month_int."-".$day_int."' ORDER BY `Timestamp` "
		                       );
		                   
		$_SESSION['yi'] = $year_int;
		$_SESSION['mi'] = $month_int;
		$_SESSION['di'] = $day_int;
		                        
		$dbhtml = new dbhtml();
		$dbif = new dbif();
		                        
		$out = $dbhtml->entries_html($argument_array);
		return $out;
	}
	function get_act_count($year,$month,$day,$UI){
		$all_acts_count = ccalendar::$dbcon->get_var("SELECT count(*) FROM `calendar_activities` WHERE UserId = '".$UI."' AND DATE(`Timestamp`) = '".$year."-".$month."-".$day."' ORDER BY `Timestamp` ");
		return $all_acts_count;
	}
	
	function create_options($num,$selected){
		$sel = '';
	  for($a = 1 ; $a <= $num ; $a++){
	    $sel.="<option value=\"".$a."\"";
	    if($selected == $a){ $sel.=" selected=\"true\" "; }
	    $sel.=" >".$a."</option>";
	  }
	  return $sel;
	}
	
}

?>