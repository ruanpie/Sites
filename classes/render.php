<?php

class render {
  private $element;
  private $attr_and_val_str;
  private $contents;
  private $html;
  
  public static $OPEN = "<";
  public static $CLOSE = ">";
  public static $SPACE_CLOSE = " >"; // combine space and close :)
  public static $SINGLE_CLOSE = " />";
  public static $CLOSING_OPEN = "</";
  public static $SPACE = " ";
  
  function ce($element,$attr_and_val_str,$contents){ // create element
    if($attr_and_val_str == ""){
      $text = self::$OPEN.$element.self::$CLOSE.$contents.self::$CLOSING_OPEN.$element.self::$CLOSE;
    }else{
      $text = self::$OPEN.$element.self::$SPACE.str_ireplace("^","'",str_ireplace("'","\"",str_ireplace("|",self::$SPACE,$attr_and_val_str))).self::$SPACE_CLOSE.$contents.self::$CLOSING_OPEN.$element.self::$CLOSE;
    }
    return $text;
  }
  
  function cse($element,$attr_and_val_str){ // create single element
    return self::$OPEN.$element.self::$SPACE.str_ireplace("^","'",str_ireplace("'","\"",str_ireplace("|",self::$SPACE,$attr_and_val_str))).self::$SINGLE_CLOSE;
  }
  
  function display_html($html){
    print $html;
  }
  
  function ce_doctype(){
    return "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Strict//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd\">";
  }
  
  
  // Example:       <table>  <tr>  <td></td>  </tr>  <table>
  function make_php_html($html){
    // look for first and last item
    $first_elem();
    
  }
}

?>