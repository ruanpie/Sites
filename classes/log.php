<?php
check_dependancy("common.php");
check_dependancy("userdetails.php");

class auth extends common {
	private $username;
	private $password;
	private $confirm_password;
	private $userid;
	private $groupname;
	private $theme;
	private $background;
	private $active;
	public $avatar_id;
	static $dbcon;
	static $page;
	private $user;
	
	function auth(){
		$dbclass = new cdbcon();
  	$db = $dbclass->return_db_con();
		auth::$page = common::get_currecnt_page();
		auth::$dbcon = $db;
	}
	
	function view_auth_user($userid,$group_id){
		$view_user = auth::$dbcon->get_row("SELECT `group`.`Group`, `userdetails`.`UserDetailsId`, `userdetails`.`Active`, `page`.`PageName`
		                                    FROM `group` 
		                                    JOIN `user`            ON ( `group`.`GroupId` = `user`.`GroupId` )  
		                                    JOIN `userdetails`     ON ( `user`.`UserId`   = `userdetails`.`UserId` )
		                                    LEFT OUTER JOIN `page` ON ( `userdetails`.`LastPageId` = `page`.`PageId` )
              		                      WHERE `user`.`UserId` = '".$userid."' AND `group`.`GroupId` = '".$group_id."' ");
		return $view_user;
	}
	
	function loginUser($username,$password,$page="app.php"){
		$error = "login failed";
		$user = $this->check_credentials($username,$password);
		if(!empty($user)){
			$groupid = @$user->GroupId;
			$userid   = @$user->UserId;
			$view_user = $this->view_auth_user($userid,$groupid);
			if(empty($view_user) || $view_user->Active == 'false'){
				return $error;
			}else {
				$_SESSION['user']         = $username;
				$_SESSION['ui']           = $userid;
				$_SESSION['user_type_id'] = $groupid;
				$_SESSION['user_type']    = $view_user->Group;
				$_SESSION['uid']          = $view_user->UserDetailsId;
				//$_SESSION['lastpage']     = $view_user->PageName;
				//common::nav($_SESSION['lastpage']);
				common::nav($page);
			}
		}else{
			return $error;
		}
	}
	
	function check_credentials($username,$password){
		$log_result = auth::$dbcon->get_row("SELECT `user`.`UserId`,`user`.`GroupId` FROM `user` WHERE Username='".$username."' AND Password='".$password."' ");
		if(empty($log_result)){ return false;}
		else{                   return $log_result;}
	}
	
	function logout(){
		session_destroy();
		common::nav("login.php");
	}
	
	function register($username,$password,$val_password,$email){
		$response = array();
		$check = $this->check_user_availability($username);
		if(!empty($check)){
			if(common::validate_entries($password,$val_password)){
				if(common::valid_email($email)){
					$user_details_id = $this->register_details($username,$password,$email);
				  if($user_details_id){
				  	$message = $this->create_register_message($user_details_id);
				  	$this->send_mail($email,$message,"Registration email verification");
				  	return $error = "0"; $reason = "";
				  }elseif($user_details_id == false){
				  	return $error = "4"; $reason = "user details creation error.";
				  }				
				}else{ $error = "3"; $reason = "invalid email address"; }
			}else{ $error = "2"; $reason = "password confirmation does not match"; }
		}else{ $error = "1"; $reason = "username in use"; }
		return common::create_function_response($error,$reason);
	}
	
	function register_details($username,$password,$email){
		$reg_result = auth::$dbcon->query("INSERT INTO `user` (`Username`,`Password`,`GroupId`) VALUES ('".$username."','".$password."','2');");     // there should always be a avatar !!!!!!! fix this
		$inserted_user_id = auth::$dbcon->insert_id;
		
		auth::$dbcon->query("INSERT INTO `playlist` (`PlaylistURL`) VALUES ('playlist.xspf') ");
		$inserted_playlist_id = auth::$dbcon->insert_id;
		
		$user_detail_sql = "INSERT INTO `userdetails` (`Active`,`ThemeId`,`BackgroundId`,`UseBackground`,`UserId`,`Opacity`,`TrsButtons`,`PlaylistId`,`AvatarId`,`Email`,`LastPageId`) VALUES ('false','1','1','1','".$inserted_user_id."','100','0','".$inserted_playlist_id."',2,'".$email."',9)";
		$reg_detail_result = auth::$dbcon->query($user_detail_sql);
		$details_id = auth::$dbcon->insert_id;
		
		if(empty($reg_result) || empty($reg_detail_result)){
			return false;
		}else{
			return $details_id;
			//common::redirect_based_on_current_page(auth::$page,"admin_users.php","register.php");
		}
	}
	
	function check_user_availability($uname){
		$un = auth::$dbcon->get_var("SELECT `Username` FROM `user` WHERE `Username` = '".$uname."' ;");		
		if(empty($un)){      return true;}
		elseif(!empty($un)){ return false;}
	}
	
	function send_mail($to,$message,$subject){
		$headers = 'From: L4MBD4'."\r\n".
		           'Reply-To: ruan800@gmail.com'."\r\n" .'X-Mailer: PHP/' . phpversion();
		mail($to, $subject, $message, $headers);
	}
	
	function change_user_group($groupid,$userid){
		auth::$dbcon->query("UPDATE `user` SET `GroupId` = '".$groupid."' WHERE UserId ='".$userid."' ");
		return auth::$dbcon->debug();
	}
	
	function create_register_message($user_details_id){
		$verification_code = md5(rand(1,10000));
		$message = "Thanks for registering. please click the link below to activate your account\r\n\r\n".constant('ACTIVE_URL')."inc/login.php?udid=".$user_details_id."&v=".$verification_code;
		auth::$dbcon->query("UPDATE `userdetails` SET `VerificationCode` = '".$verification_code."' WHERE `UserDetailsId` = '".$user_details_id."' ");	
		return $message;
	}
	
	function verify_code($user_details_id,$code){
		// find a better way of getting the userdetauks id
		$valid = auth::$dbcon->get_row("SELECT * FROM `userdetails` WHERE `UserDetailsId` = '".$user_details_id."' AND `VerificationCode` = '".$code."' ");
		if(is_object($valid)){
			$userid = $valid->UserId;
			$this->change_active($userid,'true');
			$user = auth::$dbcon->get_row("SELECT * FROM `user` WHERE `UserId` = '".$userid."' ");
			$message = "THank you for verifying your email address. you are registered as ".$user->Username."\r\nEnjoy ! :)";
			$this->send_mail($valid->Email,$message,"Verification Code confirmed");
			auth::$dbcon->query("UPDATE `userdetails` SET `VerificationCode` = NULL WHERE `UserDetailsId` = '".$user_details_id."' ");
			return true;
		}else{
			return false;
		}
	}
	
	function deleteUser($userid){ // THIS needs thinking, cant delete all cascasded items !!!
	
		//auth::$dbcon->query("DELETE FROM `".constant('DBNAME')."`.`user` WHERE `UserId` = '".$userid."' ");
		//auth::$dbcon->query("DELETE FROM `".constant('DBNAME')."`.`userdetails` WHERE `UserId` = '".$userid."' "); // should this not cascade ????
		$this->change_active($userid,'false');
	}
		
	function change_active($userid,$active){
		auth::$dbcon->query("UPDATE `userdetails` SET `Active` ='".$active."' WHERE `UserId` = '".$userid."' ");
		return true;
	}
	
	function change_user_password($userid,$oldpassword,$password,$confirm_password){
		$user = $this->open_user($userid);
		if(!empty($user)){
			if($this->check_credentials($user->Username,md5($oldpassword))){
				if($password == $confirm_password){
					auth::$dbcon->query("UPDATE `user` set `Password` = '".md5($password)."' WHERE `UserId` = '".$userid."' ");
					return true;
				}else{
					return "Confirmation password does not match";
				}
			}else{
				return "old password is bogus";
			}
		}
	}
	
	function reset_user_password($userid){
		$RawPassword = common::generatePassword(9,8);
		$random_password = md5($RawPassword);
		$userdetails = userdetails::open_userdetails_db($userid,auth::$dbcon);
		if(!empty($userdetails)){
			$link = constant('ACTIVE_URL')."modules/u.php?a=v&u=".$userid."&s=".$random_password;
			$revoke_url = constant('ACTIVE_URL')."modules/u.php?a=rv&u=".$userid;
			$message = "A request was completed to reset your password.\r\nPlease click the link below if you wish to change your password.\r\n\r\n".$link.
			           "Your randomly generated Password is : ".$RawPassword.
			           "\r\n If you have not requested a password change please click : ".$revoke_url;
			$this->send_mail($userdetails->Email,$message,"Reset Password");
			userdetails::update_verification_code_db($userid,$random_password,auth::$dbcon);
		}
		
	}
	
	function confirm_user_reset_password($userid,$secret){
		$userdetails = userdetails::open_userdetails_db($userid,auth::$dbcon);
		if(!empty($userdetails)){
			$verification_code = auth::$dbcon->get_var(" SELECT VerificationCode FROM `userdetails` WHERE `UserId` = '".$userid."' ");
			if($verification_code == $secret){
				auth::$dbcon->query("UPDATE `user` set `Password` = '".$secret."' WHERE `UserId` = '".$userid."' ");
				$this->revoke_password_reset($userid);
			}
		}
	}
	
	function revoke_password_reset($userid){
		auth::$dbcon->query("UPDATE `userdetails` SET `VerificationCode` = '' WHERE `UserId` = '".$userid."' ");
	}
	
	function update_avatar($avatar_id){
		if(auth::$dbcon->query("UPDATE `userdetails` SET `AvatarId` = '".$avatar_id."' WHERE `UserDetailsId` = '".$_SESSION['uid']."' ")){
			return true;
		}else{
			return false;
		}
	}
	
	function open_user($userid){
		$res = auth::$dbcon->get_row("SELECT * from `user` WHERE `UserId` = '".$userid."' ");
		if(empty($res)){ return false;}
		else{            return $res;}
	}
	
	function open_user_details($userid){
		$res = auth::$dbcon->get_row("SELECT * from `userdetails` WHERE `UserId` = '".$userid."' ");
		if(empty($res)){ return false;}
		else{            return $res;}
	}
	
	function view_all_users($limit){
		$view_users = auth::$dbcon->get_results("SELECT * FROM `user` JOIN `userdetails` ON (`user`.`UserId` = `userdetails`.`UserId`) LIMIT ".$limit);
		return $view_users;
	}
	
}

$a = new auth();

if($_POST){
	// login page
	if( !empty($_POST['log_username']) && !empty($_POST['log_password'])){
		$username = $_POST['log_username'];
		$pass = md5($_POST['log_password']);
		if(defined('LOGGED_IN_PAGE')){
			$error = $a->loginUser($username,$pass,constant('LOGGED_IN_PAGE'));
		}else{
			$error = $a->loginUser($username,$pass);
		}
	}
	
	// register
	if( !empty($_POST['reg_username']) && !empty($_POST['reg_password']) && !empty($_POST['val_reg_password']) && !empty($_POST['reg_email']) ){
		$ruser    = $_POST['reg_username'];
		$rpass    = md5($_POST['reg_password']);
		$valrpass = md5($_POST['val_reg_password']);
		$remail   = $_POST['reg_email'];
		$reg_response = $a->register($ruser,$rpass,$valrpass,$remail);
	
	}
	
	if(isset($_POST['log_status']) && !empty($_POST['log_status'])){
		$log_status = $_POST['log_status'];
		if($log_status == "logout"){
			$a->logout();
		}
	}
	
	if(!empty($_POST['user_id']) && !empty($_POST['change_password'])){
		$oldpassword      = addslashes($_POST['old_password_'.$_POST['user_id']]);
	  $new_password     = addslashes($_POST['new_password_'.$_POST['user_id']]);
	  $confirm_password = addslashes($_POST['confirm_password_'.$_POST['user_id']]);
		if( isset($new_password) && !empty($new_password) && isset($confirm_password) && !empty($confirm_password) ){
	    $a->change_user_password($_POST['user_id'],$oldpassword,$new_password,$confirm_password);
		}	
	}
	
}
?>