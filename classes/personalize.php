<?php
check_dependancy("theme.php");

class personalization extends theme {  
  private $theme;
  private $file_id;
  private $opacity;
  private $new_setting;
  private $redirect_page;
  static $dbcon;
  
  function personalization(){
  	$dbclass = new cdbcon();
  	$db = $dbclass->return_db_con();
  	personalization::$dbcon = $db;
  }
  
  function change_user_theme($themeCssRef){
		$ThemeId = personalization::$dbcon->get_var("SELECT `ThemeId` FROM `theme` WHERE `cssRef` = '".$themeCssRef."' ");
		personalization::$dbcon->query("UPDATE `userdetails` SET `ThemeId` = '".$ThemeId."' WHERE `UserId` = '".$_SESSION['ui']."' ");
		theme::set_session_data($_SESSION['FID'],$_SESSION['fname'],$themeCssRef,$_SESSION['opacity'],$_SESSION['trans_buttons'],$_SESSION['av_file_name']);
		//common::nav(constant('INC')."personalize.php");
	}
	
	function change_user_background($file_id,$redirect_page,$tab_nmr){
		$file_id_Str = (string) $file_id;
		if($file_id_Str == "NULL"){
		  personalization::$dbcon->query("UPDATE `userdetails` SET `UseBackground` = '0', `BackgroundId` = '1' WHERE `UserId` = '".$_SESSION['ui']."'");
		  theme::set_session_data("NULL","NULL",$_SESSION['theme'],$_SESSION['opacity'],$_SESSION['trans_buttons'],$_SESSION['av_file_name']);
		}else{
		  $backgroundRow = personalization::$dbcon->get_row("SELECT BackgroundId FROM `background` WHERE `FileId` = '".$file_id."'");
  	  if(!empty($backgroundRow->BackgroundId)){
  	    $backId = $backgroundRow->BackgroundId;
  	  }else{
  	    $backId = personalization::$dbcon->query("INSERT INTO `background` (`FileId`) VALUES ('".$file_id."')");
  		  $backId = personalization::$dbcon->insert_id;
  	  }
  	  $fileName = personalization::$dbcon->get_var("SELECT Name from `file` WHERE `FileId` = '".$file_id."'");
  		personalization::$dbcon->query("UPDATE `userdetails` SET `BackgroundId` = '".$backId."',`UseBackground` = '1' WHERE `UserId` = '".$_SESSION['ui']."' ");
  		theme::set_session_data($file_id,$fileName,$_SESSION['theme'],$_SESSION['opacity'],$_SESSION['trans_buttons'],$_SESSION['av_file_name']);
		}
		if($tab_nmr == ''){ common::nav(constant('INC').$redirect_page); } else { common::navparams(constant('INC').$redirect_page,"?r=".$tab_nmr); }
	}
	
	function change_user_oppacity($opacity){
		personalization::$dbcon->query("UPDATE `userdetails` SET `Opacity` = '".$opacity."' WHERE `UserId` = '".$_SESSION['ui']."' ");
		theme::set_session_data($_SESSION['FID'],$_SESSION['fname'],$_SESSION['theme'],$opacity,$_SESSION['trans_buttons'],$_SESSION['av_file_name']);
		//common::nav(constant('INC')."personalize.php");
	}
	
	function change_transparent_button_setting($new_setting){
	  personalization::$dbcon->query("UPDATE `userdetails` SET `TrsButtons` = '".$new_setting."'  WHERE `UserId` = '".$_SESSION['ui']."' ");
	  theme::set_session_data($_SESSION['FID'],$_SESSION['fname'],$_SESSION['theme'],$_SESSION['opacity'],$new_setting,$_SESSION['av_file_name']);
	  //common::nav(constant('INC')."personalize.php");
	} 
	
	function change_avatar($fileName){
    theme::set_session_data($_SESSION['FID'],$_SESSION['fname'],$_SESSION['theme'],$_SESSION['opacity'],$_SESSION['trans_buttons'],$fileName);
	}
	
}
/*
if($_POST){
  $per = new personalization();
  if(    @$_POST['personalize_action'] == "trs_button"){ $per->change_transparent_button_setting(strip_tags($_POST['trs_button']));}
  elseif(@$_POST['personalize_action'] == "sel_theme") { $per->change_user_theme(strip_tags($_POST['sel_theme']));}
  elseif(@$_POST['personalize_action'] == "background"){ $per->change_user_background(strip_tags($_POST['bcg']),'personalize.php','');}
  elseif(@$_POST['personalize_action'] == "opacity")   { $per->change_user_oppacity(strip_tags($_POST['user_opacity'])); }
  
  $tab_nmr = "0";
	if(isset($_POST['r'])){ $tab_nmr = $_POST['r'];}
	
  if(@$_POST['upload_change_background'] == 'background'){
    $per->change_user_background(strip_tags($_POST['bcg']),'upload.php',$tab_nmr);
    $nav_page = 'upload.php';
  }elseif(@$_POST['upload_change_background'] == 'default'){
    $nav_page = 'upload.php';
  }
}
*/
?>