<?php
check_dependancy("common.php");

// try creating configs when not present.

class server_settings  extends common {
	private $user;
	private $pass;
	private $name; 
	private $host;
	private $online_path;
	private $local_path;
	private $active_path;
	
	function save_settings($user,$pass,$name,$host){ // DONT INDENT CODE BELOW !!! or fix the code :)
$new_setting = "<?php
define(\"DBUSER\",'$user');
define(\"DBPASS\",'$pass');
define(\"DBNAME\",'$name');
define(\"DBHOST\",'$host');?>";
		try{
			//$myFile = ;
			$fh = fopen(constant('DBSETTINGS'),'w') or die("can't open database settings file");
			fwrite($fh,$new_setting);
			fclose($fh);
			return true;
		}catch (Exception $e){
			return $e;
		}
	}
	
	function get_settings(){
		if(file_exists(constant('DBSETTINGS'))){
		  $db_settings_string = file_get_contents(constant('DBSETTINGS'));
		  $rem_str = str_replace("?>","",str_replace("<?php","",$db_settings_string));
		  $lines = array();
  		  $lines = explode(";",$rem_str);
  		
  		  $settings = array();
  		  array_push($settings,common::get_val_with_needle($lines[0],"'"));
  		  array_push($settings,common::get_val_with_needle($lines[1],"'"));
  		  array_push($settings,common::get_val_with_needle($lines[2],"'"));
  		  array_push($settings,common::get_val_with_needle($lines[3],"'"));
  		  return $settings;
		}else{
		  return "Database Configuration does not exist. Please contact admin\r\n";
		}
	}
	
	function save_path_settings($online_path,$local_path,$active_path){
	  echo $online_path."<br />" ;
	  echo $local_path."<br />" ;
	  echo $active_path."<br />" ;
	  
	   $new_path_setting = "<?php
define(\"ONLINE_PATH\",'$online_path');
define(\"LOCAL_PATH\", '$local_path');
define(\"ACTIVE_PATH\",constant('$active_path'));?>";
    try{
      $fh = fopen(constant('PATHSETTINGS'),'w') or die("can't open path settings file");
      fwrite($fh,$new_path_setting);
      fclose($fh);
      return true;
    }catch (Exception $e){
      return $e;
    }
	}
	
	function get_path_settings(){
    if(file_exists(constant('PATHSETTINGS'))){
      $path_settings_string = file_get_contents(constant('PATHSETTINGS'));
      $rem_str = str_replace("?>","",str_replace("<?php","",$path_settings_string));
      $lines = array();
      $lines = explode(";",$rem_str);
      
      $paths = array();
      array_push($paths,common::get_val_with_needle($lines[0],"'"));
      array_push($paths,common::get_val_with_needle($lines[1],"'"));
      array_push($paths,common::get_val_with_needle($lines[2],"'"));
      return $paths;
    }else{
      return "Path Configuration does not exist. Please contact admin\r\n";
    }
	}

}
?>