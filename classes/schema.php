<?php
check_dependancy("common.php");

class schema_install  extends common {
  private $con;
  private $dbname;
  
  function create_database($con,$dbname){
    mysql_query("CREATE DATABASE `".$dbname."` DEFAULT CHARACTER SET utf8 COLLATE utf8_bin",$con);
  }
  
  function create_db_tables($con,$dbname){
    $db_tables = file_get_contents(constant('INSTALL').'dbcreate.sql');
    $tables = explode("CREATE TABLE IF NOT EXISTS ",$db_tables);
    $table_count = 0;
    foreach ($tables as $table){
      if($table > ''){
        $query_string = "CREATE TABLE IF NOT EXISTS `".$dbname."`.".$table;        
        mysql_query($query_string,$con);
        $table_count++;
      }
    }
    
    try{
			$myFile = constant('INSTALL').'schema_count.txt';
			$fh = fopen($myFile,'w') or die("can't open file");
			fwrite($fh,$table_count);
			fclose($fh);
		}catch (Exception $e){
			return $e;
		}
  }
  
  function insert_db_tables($con,$dbname){
    $db_data = file_get_contents(constant('INSTALL').'dbinsert.sql');
    $tables_data = explode("INSERT INTO",$db_data);
    foreach ($tables_data as $table_data){
      if($table_data > ''){
        $query_string = "INSERT INTO `".$dbname."`.".$table_data;
        mysql_query($query_string,$con);
      }
    }
  }
  
  function re_install($con){
    mysql_query("DROP DATABASE `".constant('DBNAME')."` ",$con);
    $this->create_database($con,constant('DBNAME'));
    $this->create_db_tables($con,constant('DBNAME'));
    $this->insert_db_tables($con,constant('DBNAME'));
  }
  
}

class schema {
  private $dbname;
  static $dbcon;
  
  function schema(){
  	$con = mysql_connect(constant('DBHOST'),constant('DBUSER'),constant('DBPASS'),constant('DBNAME'));
  	schema::$dbcon = $con;
  }
  
  function get_default_tables($dbname){
			$expected_tables = array("db" => $dbname,
															 "background",
															 "calendar_activities",
															 "chat",
															 "chat_message",
															 "file",
															 "group",
															 "group_buttons",
															 "group_page",
															 "button",
															 "page",
															 "planning",
															 "posted_links",
															 "theme",
															 "theme_details",
															 "user",
															 "userdetails",
															 "playlist",
															 "org_topic",
															 "org_keyword",
															 "music_player",
															 "page_picture",
															 "status",
															 "rss_feed",
															 "rss_feed_entry",
															 "scope",
															 "user_rss_feeds"
															 );
    return $expected_tables;
  }
  
  function get_tables($dbcon,$dbname){
    $tables_array = array();
    $tables_array['db'] = $dbname;
    $table_result = mysql_query("SHOW TABLES FROM `".$dbname."` ",$dbcon);
    for($a = 0;$a < @mysql_num_rows($table_result);$a++){
      $tables = mysql_fetch_array($table_result);
      $key = 'Tables_in_'.$dbname;
      array_push($tables_array,$tables[$key]);
    }
    return $tables_array;
  }
  
  function get_databases($dbcon){
    $database_array = array();
    $databases_result = mysql_query("SHOW DATABASES",$dbcon);
    for($a = 0;$a < mysql_num_rows($databases_result);$a++){
      $databases = mysql_fetch_array($databases_result);
      array_push($database_array,$databases['Database']);
    }
    return $database_array;
  }
  
  function is_database_valid($dbname){
  	$db_arg = schema::$dbcon;
  	schema::is_database_valid_db($db_arg,$dbname);
  }
  
  function is_database_valid_db($dbcon_arg,$dbname){
    $tables = schema::get_tables($dbcon_arg,$dbname);
    $expected_tables = schema::get_default_tables($dbname);
    if( (count( array_diff($tables,$expected_tables) )) > 0 ){
      return false;
    }else{
      return true;
    }
  }
  
  function is_schema_installed(){
    if($current_page != "install.php" && $installed == false){
      common::nav(constant('INSTALL')."install.php");
    }
  }
  
  function get_schema_count(){
  	$schema_count = file_get_contents(constant('INSTALL')."schema_count.txt");
		return $schema_count;
  }
  
}
?>