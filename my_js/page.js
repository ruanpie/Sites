var val, param;

function func(){
	$('my_settings_frm').onsubmit = 
	function(){
		$('my_settings_frm').target = 'upload_target';
	}
}	

window.addEvent('domready',
	function(){ 
	  $('menu').style.display = '';
		track_scroll();
		init_loading();
		load_starting_page();
		load_avatar();
	});

function init_loading(){
	$('load_img').style.top = (this.getWindow().getSize().x * 0.25)+'px';
	$('load_img').style.left = (this.getWindow().getSize().x * 0.50)+'px';
	$('load_img').style.display='';
}

function load_starting_page(){
	ajax_get_call('../page_mods/pages_mod.php','?a=pn',undefined,'loader(\'p\',response,undefined)');
}

function load_avatar(){
  ajax_get_call('../page_mods/pages_mod.php','?a=ua',undefined,'$(\'user_avatar_img\').set(\'src\',response);',undefined);
}


function loader(param,val,next_action){
	var action_str = '';
	if(next_action != undefined){
		action_str = next_action;
	}
	ajax_get_call('../modules/page_render.php','?'+param+'='+val,'full_page',loader_completed_cmd(val),action_str+'loading_on();');
	$('cp').value = val;
}

function load_page(val){
	loader('p',val,undefined);	
}

function lcp(){
	loader('p',$('cp').value,undefined);
}

function loader_completed_cmd(val){
	return "if(response > ''){ajax_get_call('../page_mods/pages_mod.php','?a=pa&p="+val+"',undefined,'eval(response);');} load_page_picture(); loading_off();";
}





function loading_on(){
	$('dim').style.display = '';
}

function loading_off(){
	$('dim').style.display = 'none';
}

function load_page_picture(){
	ajax_get_call('../page_mods/pages_mod.php','?a=pp',undefined,'$(\'page_picture\').set(\'src\',response);',undefined);
}


