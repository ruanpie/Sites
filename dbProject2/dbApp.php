<?php
require("modules/connector.php");
if($_SESSION && $_POST) {
  if(@$_POST['disconnect'] == 'true'){
    db_con_close($con);
  }
  if(!empty($_SESSION['db_host']) && !empty($_SESSION['db_connected']) &&!empty($_SESSION['db_user'])){
    $con = db_con($_SESSION['db_host'],$_SESSION['db_user'],$_SESSION['db_pass']); //connect trhough session 
  }else{ 
    if(!empty($_POST['user']) && !empty($_POST['host'])){
      $con = db_con($_POST['host'],$_POST['user'],$_POST['pass']);
    }
  }
}
?>
<!DOCTYPE html
PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" 
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
<title>Database Application.</title>
	<script type="text/javascript" src="js/moo.js"></script>
	</script><script type="text/javascript" src="js/script.js"></script><link rel="stylesheet" href="css/main.css" />
	<script type="text/javascript">
	window.addEvent('domready', function() {
		get_databases();
		//$('sel_database').innerHTML = "<option>--</option>";
		pop_db_name();
	 });
	 </script>
</head>
<body>

<?php
// mysql_select_db("cms");
// $result = mysql_query("SHOW TABLES");
//while ($row = mysql_fetch_array($result,MYSQL_NUM)){
//     print $row[0]."<br />" ;
//}
//mysql_free_result($result);
?> 

<?php 
//$tblref = mysql_query("SHOW TABLES",$con);
//$row = mysql_fetch_row($tblref);
//print_r($row);
?>

<form name="dbform" action="dbApp.php" method="post">

<input type="hidden" id="database_selected" />
<input type="hidden" id="table_selected"/>
<input type="hidden" id="selected_table_number" >

<?php if($_SESSION["db_connected"] == false || !isset($_SESSION["db_host"])){ ?>
<div align="left">
	<table style="border:1px solid black;float:left;">
		<tr><td colspan="2"><div align="center"><b>Connect local:</b></div></td></tr>
		<tr><td><div align="right">Username:</div></td><td><div align="left"><input type="text" name="user" value="<?php print DB_APP_USER;?>" /></div></td></tr>
		<tr><td><div align="right">Password:</div></td><td><input type="password" name="pass" value="<?php print DB_APP_PASS;?>" /></td></tr>
		<tr><td><div align="right">Host:</div></td><td><input type="text" name="host" value="<?php print DB_APP_HOST;?>" /></td></tr>
		<tr><td colspan="2"><div align="center"><button type="submit">Connect</button></div><td></tr>
	</table>
</div>	
	<?php }elseif ($_SESSION['db_connected'] == true){ ?>
	<a name="top"></a>
	<table style="border-collapse:collapse;" border="1" cellpadding="4" cellspacing="4" >
		<tr>
		<td valign="top" class="rec_header">
			<i>Host</i> :<b>
			<?php print $_SESSION['db_host']; ?>
			</b><i>Current User </i>:<b>
			<?php print $_SESSION['db_user']; ?>
			</b>
				<div align="right">
					<button type="button" onclick="this.nextSibling.value = 'true';document.dbform.submit();" >Disconnect</button><input type="hidden" value="" name="disconnect">
				</div>
			</td>
		</tr>
		<tr>
			<td valign="middle">
				<span id="record_length" style="display:none;">
					<a href="dbApp.php#actions">Actions<img src="img/down_arrow.gif"></a>
					row amount
					<select id="row_amount" >
						<option value="20" selected="selected" >20</option>
						<option value="50" >50</option>
						<option value="100" >100</option>
						<option>All</option>
					</select>
					<button type="button" ><<</button>
					<button type="button" ><</button>
						From: <input type="text" size="5" id="records_selected_from" />
						To:   <input type="text" size="5" id="records_selected_to" />
						Total:<input type="text" size="5" id="amount_records" />
					<button type="button" >></button>
					<button type="button" >>></button>
				</span>
			</td>
		</tr>
		<tr>
			<td colspan="2">
				<div align="left">
					<table style="border-collapse:collapse;" border="1">
						<tr>
							<td valign="top">
								<table id="sel_database">
								</table>
								<button type="button" onclick="select_database($('sel_database').value);" id="sel1" style="display:none;">select</button>
								<table id="sel_table">
								</table>
								<button type="button" onclick="select_table(this.previousSibling.value)" id="sel2" style="display:none;">select</button>
							</td>
							<td valign="top">
								<table>
									<tr>
										<td id="record_table"></td>
									</tr>
								</table>
								<button type="button" onclick="select_table(this.previousSibling.value)" id="sel2" style="display:none;">select</button>
							</td>
						</tr>
					</table>
					<img id="loading_image" style="display:none;float:right;" src="img/ajax-loader.gif"/>
					<span id="l_time" style="display:none;"></span>
				</div>
			</td>
		</tr>
		<tr>
			<td colspan="2">
				<!--<table align="center" id="record_table" style="border-collapse:collapse;" border="1" cellpadding="4" cellspacing="4"></table>-->
			</td>
		</tr>
		<tr>
			<td colspan="2" >
				<table id="actions" align="center" style="border-collapse:collapse;" border="1" cellpadding="4" cellpadding="4">
					<tr><td colspan="2" class="rec_header"><div align="center"><a href="#" name="actions">Actions:<a></div></td></tr>
					<tr><td>Create Database:</td><td>Name:<input type="text" id="new_db_name" />
						<button id="create_db_button" type="button" onclick="create_db($('new_db_name').value);">Create</button></td></tr>
					<tr><td>Create Table:</td><td><div id="wtf"></div>
					<table>
						<tr>
							<td>Name: </td><td><input type="text" id="new_table" /><br /></td>
							<td>Field amount: </td><td><input type="text" id="num_fields" /></td>
							<td><button id="create_table" type="button" onclick='start_creating_new_table($("new_table").value,$("num_fields").value)'>Create</button></td>
						</tr>
					</table>
						</td></tr>
					<tr id="new_table_row" style="display:none;">
						<td>&nbsp;</td>
						<td id="creation_area"></td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td colspan="2">
				<a href="dbApp.php#top">Back to top <img src="img/up_arrow.gif" /></a>
			</td>
		</tr>
	</table>
<?php } ?>
</form>
<table  style="display:none;">
	<tr><td>Host :</td><td><?php print DB_APP_HOST;?></td></tr>
	<tr><td>User :</td><td><?php print DB_APP_USER;?></td></tr>
	<tr><td>Pass :</td><td><?php print DB_APP_PASS;?></td></tr>
	<tr><td>Selected database :</td><td><?php print DB_APP_SEL_DB;?></td></tr>
	<tr><td>Selected table :</td><td><?php print DB_APP_SEL_TABLE;?></td></tr>
	<tr><td>Connected :</td><td><?php print $_SESSION['db_connected'];?></td></tr>
</table>
<?php
require("includes/data_types.html");
require("includes/colation.html");
?>
<select id="default_values" style="display:none;">
	<option value=""> </option>
	<option value="USER_DEFINED">As defined:</option>
	<option value="NULL">NULL</option>
	<option value="CURRENT_TIMESTAMP">CURRENT_TIMESTAMP</option>
</select>
<select id="attributes" style="font-size: 70%;display:none;">               
	<option selected="selected" value=""/>
	<option value="BINARY">BINARY</option>
	<option value="UNSIGNED">UNSIGNED</option>
	<option value="UNSIGNED ZEROFILL">UNSIGNED ZEROFILL</option>
	<option value="on update CURRENT_TIMESTAMP">on update CURRENT_TIMESTAMP</option>
</select>
<select id="index" style="display:none;">
	<option value=""> </option>
	<option title="Primary" value="PRIMARY KEY">PRIMARY</option>
	<option title="Unique" value="unique_0">UNIQUE</option>
	<option title="Index" value="index_0">INDEX</option>
	<option title="Fulltext" value="fulltext_0">FULLTEXT</option>
</select>
<select id="storage_engines"  style="display:none;">
    <option title="Hash based, stored in memory, useful for temporary tables" value="MEMORY">MEMORY</option>
    <option title="Supports transactions, row-level locking, and foreign keys" value="InnoDB">InnoDB</option>
    <option selected="selected" title="Default engine as of MySQL 3.23 with great performance" value="MyISAM">MyISAM</option>
    <option title="/dev/null storage engine (anything you write to it disappears)" value="BLACKHOLE">BLACKHOLE</option>
    <option title="Collection of identical MyISAM tables" value="MRG_MYISAM">MRG_MYISAM</option>
    <option title="CSV storage engine" value="CSV">CSV</option>
    <option title="Archive storage engine" value="ARCHIVE">ARCHIVE</option>
</select>
</body>
</html>