<?php
session_start();
define('DBUSER','root');
define('DBPASS','');
define('DBNAME','testdb');
define('DBHOST','localhost');

require('class_checker.php');
require('render.php');
require('ez_sql_core.php');
require('ez_sql_mysql.php');
require('cdbcon.php');
require('common.php');
require('dbif.php');
require('dbhtml.php');

$dbhtml = new dbhtml();

function page($table_offset,$button_offset,$dbhtml,$g){
  $_SESSION['table_offset'] = $table_offset;
  $_SESSION['button_offset'] = $button_offset;
  $argument_array = array("table_attributes" => $_SESSION['table_attributes'],
                          "action_array"     => $_SESSION['action_array'],
                          "table_limit"      => $_SESSION['table_limit'],
                          "sql_table"        => $_SESSION['sql_table'],
                          //"table_page"       => common::get_currecnt_page(),
                          "table_offset"     => $_SESSION['table_offset'],
                          "button_offset"    => $_SESSION['button_offset'],
                          "app_get_urlr"     => $g
                          );
  $entries_html = $dbhtml->entries_html($argument_array);
  return $entries_html;
}

function set_or_get_session($g){
  value_set_then_leave('table_attributes',"border='1'|style='border-collapse:collapse;'|cellpadding=8px|cellspacing=8px");
  value_set_then_leave('action_array',array("edit","delete"));
  value_set_then_leave('table_limit',10);
  value_set_then_leave('sql_table','test_users');
  value_set_then_leave('table_offset',0);
  value_set_then_leave('button_offset',0);
  value_set_then_leave('app_get_urlr',$g);  
}

function value_set_then_leave($v,$new_val){
  if(!isset($_SESSION[$v])){
    $_SESSION[$v]=$new_val;
  }
}

if( isset($_GET) && isset($_GET['a']) ){
  switch($_GET['a']){
    case 'start':
      set_or_get_session($_GET);
      print page($_SESSION['table_offset'],$_SESSION['button_offset'],$dbhtml,$_GET);
      break;
    case 'next_results':
      if( isset($_GET['o']) && isset($_GET['bo']) ){
        print page($_GET['o'],$_GET['bo'],$dbhtml,$_GET);
      }
      break;
  }
}

?>