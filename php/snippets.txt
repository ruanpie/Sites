CAPS LOCK - JS

JS : 
    function capLock(e){
        kc = e.keyCode?e.keyCode:e.which;
        sk = e.shiftKey?e.shiftKey:((kc == 16)?true:false);
        if(((kc >= 65 && kc <= 90) && !sk)||((kc >= 97 && kc <= 122) && sk))
            document.getElementById('divCapsLock').style.visibility = 'visible';
        else
            document.getElementById('divCapsLock').style.visibility = 'hidden';
    }

HTML : 

<body onLoad="initpage()" onKeyPress="capLock(event)">

<div id="divCapsLock" style="visibility:hidden">Your CAPS LOCK may be on. Passwords must be entered with the correct UPPER and lower case.</div>

<input type="password" name="pword" id="pword" size="20" onKeyPress="capLock(event)"> 

