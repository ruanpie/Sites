<?php
session_start();
//session_destroy();

print_r($_SESSION);

if($_POST){
	if(isset($_POST['username']) && isset($_POST['password']) && isset($_POST['host'])){
		$_SESSION['u'] = $_POST['username'];
		$_SESSION['p'] = $_POST['password'];
		$_SESSION['h'] = $_POST['host'];
	}
	
	if(isset($_POST['dbname'])){
		$_SESSION['d'] = $_POST['dbname'];
	}
	
}

define('DBUSER',@$_SESSION['u']);
define('DBPASS',@$_SESSION['p']);
define('DBNAME',@$_SESSION['d']);
define('DBHOST',@$_SESSION['h']);

require('class_checker.php');
require('render.php');
require('ez_sql_core.php');
require('ez_sql_mysql.php');
require('cdbcon.php');
require('common.php');
require('dbif.php');
require('dbhtml.php');
$current_page = common::get_currecnt_page();
$dbclass = new cdbcon();
$dbif = new dbif();
$dbhtml = new dbhtml();


if( isset($_SESSION['u']) && isset($_SESSION['p']) && isset($_SESSION['h']) && !isset($_SESSION['d']) &&
	  $_SESSION['u'] > '' && $_SESSION['p'] > '' && $_SESSION['h'] > ''){
	 	$database_html = $dbhtml->all_dbs_html();
	  $body_html = 
	  	render::ce("form","action='".$current_page."'| method='post' ",
	  		render::ce("table","",
	  			render::ce("tr","",
	  				render::ce("td","","Database : ").render::ce("td","",render::ce("select","name='dbname'",$database_html))
	  			).
	  			render::ce("tr","",
	  				render::ce("td","colspan='2'| align='right'",render::ce("button","","Connect"))
	  			)
	  		)
	  	);
	  
}elseif( isset($_SESSION['u']) && isset($_SESSION['p']) && isset($_SESSION['d']) && isset($_SESSION['h']) &&
	  $_SESSION['u'] > '' && $_SESSION['p'] > '' && $_SESSION['d'] > '' && $_SESSION['h'] > '' &&
	  !isset($_SESSION['tbl'])){	
	  
	$entries_html = $dbhtml->entries_html($argument_array);
	$database_html = $dbhtml->all_dbs_html();	
	$body_html = 
	render::ce("table","",
		render::ce("tr","",
			render::ce("td","","Database : ").
			render::ce("td","",render::ce("select","",$database_html)).
			render::ce("td","","Table : ").
			render::ce("td","",render::ce("select","","-----")) // populate tables after database was chosen
		)
	)
	  
}elseif( isset($_SESSION['u']) && isset($_SESSION['p']) && isset($_SESSION['d']) && isset($_SESSION['h']) &&
	  $_SESSION['u'] > '' && $_SESSION['p'] > '' && $_SESSION['d'] > '' && $_SESSION['h'] > '' &&
	  isset($_SESSION['tbl']) && $_SESSION['tbl'] > '' ){	
	 
	$dbcon = $dbclass->return_db_con();
	
	$table_attributes = "border='1'|style='border-collapse:collapse;'|cellpadding=8px|cellspacing=8px";
	$action_array     = array("edit","delete");
	$table_limit      = 10;
	$sql_table        = "user";
	if(isset($_GET['o']) && $_GET['o'] > 0){
		$table_offset = $_GET['o'];
	}else{
		$table_offset = 0;
	}
	if(isset($_GET['bo']) && $_GET['bo']){
		$button_offset = $_GET['bo'];
	}else{
		$button_offset = 0;
	}
	$argument_array = array("table_attributes"=> $table_attributes,
	                        "action_array"    => $action_array,
	                        "table_limit"     => $table_limit,
	                        "sql_table"       => $sql_table,
	                        "table_page"      => $current_page,
	                        "table_offset"    => $table_offset,
	                        "button_offset"   => $button_offset,
	                        "app_get_urlr"    => $_GET
	                        );
	
	$entries_html = $dbhtml->entries_html($argument_array);
	$database_html = $dbhtml->all_dbs_html();	
	$body_html = 
	render::ce("table","",
		render::ce("tr","",
			render::ce("td","","Database : ").
			render::ce("td","",render::ce("select","",$database_html)).
			render::ce("td","","Table : ").
			render::ce("td","",render::ce("select","","-----")) // populate tables after database was chosen
		)
	).$entries_html;
	
}else{

	// SHOW CONNECT AREA
	$body_html = 
	render::ce("form","method='post'| action='".$current_page."' ",
		render::ce("table","",
			render::ce("tr","",render::ce("td","colspan='8'","Database Application Settings")).
			render::ce("tr","",render::ce("td","align='right'","Username : ").render::ce("td","",render::cse("input","type='text'|name='username'"))).
			render::ce("tr","",render::ce("td","align='right'","Password : ").render::ce("td","",render::cse("input","type='text'|name='password'"))).
			render::ce("tr","",render::ce("td","align='right'","Host : ").    render::ce("td","",render::cse("input","type='text'|name='host'"))).
			render::ce("tr","",render::ce("td","colspan='8'|align='right'",   render::ce("button","","Connect")))
		)
	);
}

$page = 
render::ce("html","",
	render::ce("head","","").
	render::ce("body","",
		$body_html
	)
);
print $page;

?>

