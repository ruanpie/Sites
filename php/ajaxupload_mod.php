<?php

list($name,$result) = upload($_FILES['file']);

if($name) { // Upload Successful
	$details = stat("image_uploads/$name");
	$size = $details['size'] / 1024;
	print json_encode(array(
		"success"	=>	$result,
		"failure"	=>	false,
		"file_name"	=>	$name,	// Name of the file - JS should get this value
		"size"		=>	$size	// Size of the file - JS should get this as well.
	));
	
} else { // Upload failed for some reason.
	print json_encode(array(
		"success"	=>	false,
		"failure"	=>	$result,
	));
	
}


function upload($file){
  $fileName       = $file["name"];
  $filetype       = $file["type"];
  $temp_file_name = $file["tmp_name"];
  if(upload_file($temp_file_name,$fileName)){
    return true;
  }else{
    return false;
  }
}
  
function upload_file($temp_file_name,$name){
  if( move_uploaded_file($temp_file_name,$name) ){
    return true;
  }else{
    return false;
  }
}

  
?>