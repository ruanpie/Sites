var url = 'http://demos111.mootools.net/demos/Ajax.Advanced/ajax.php';
 
 
var fn = function() {
	var box = $(this.options.update).getParent();
	var fx = new Fx.Style(box, 'background-color', {
		duration: 800,
		transition: Fx.Transitions.Quad.easeOut
	}).start('#fb9393', '#f8f8f8');
}
 
 
$('Request').addEvent('click', function() {
	var options = {}
	$('myForm').getFormElements().each(function(el){
		var name = el.name;
		var value = el.getValue();
 
		if (name == 'onComplete') {
			if (value) options['onComplete'] = fn;
			return;
		}
		if (value === false || !name || el.disabled) options[value] = false;
		else if (name == 'update') options[name] = 'log_res_' + value;
		else options[value] = true;
	});
	$('myForm').send(options);
});
 
 
// Reset function for when refreshing the page
var resetBar = function() {
	$('update').options[0].selected = true;
	$$('input[type="checkbox"]').each(function(checkbox) {
		if (checkbox.id == 'data') checkbox.setProperty('checked', true);
		else checkbox.setProperty('checked', false);
	});
}
resetBar();
 
