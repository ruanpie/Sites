<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Ctrl extends CI_Controller {

  public function __construct(){
    parent::__construct();
  }

  public function view($page='index'){
    if ( ! file_exists('application/views/'.$page.'.php') ){
      show_404();
    }
    $data['title'] = '';
    $this->load->view("template/header",$data);
    $this->load->view($page,$data);
    $this->load->view("template/footer",$data);
  }
  
  public function create($application_name){
    
    $this->view($page='index');
  }

}

?>